import 'package:ecommerce/ui/setting/store/setting_store_activity.dart';
import 'package:ecommerce/ui/setting/user/setting_user_activity.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SettingActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  SettingActivity(this.isLanguage, this.isCurrency);

  @override
  _SettingActivityState createState() => _SettingActivityState(isLanguage, isCurrency);
}

class _SettingActivityState extends State<SettingActivity> with SingleTickerProviderStateMixin{
  bool isLanguage;
  bool isCurrency;

  _SettingActivityState(this.isLanguage, this.isCurrency);

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: 2,
      vsync: this,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: _tabController.length,
        child: Scaffold(
          appBar: AppBar(
            title: Padding(
              padding: EdgeInsets.all(MediaQuery.of(context).size.width / 7),
              child: Text((isLanguage != true)?"Akun Saya":"My Account", style: GoogleFonts.poppins(
                  textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
            ),
            leading: Container(),
            backgroundColor: Color(0xffe3cea3),
          ),
          body: Column(
            children: [
              new TabBar(
                controller: _tabController,
                indicatorColor: Color(0xff872934),
                labelColor: Color(0xff872934),
                unselectedLabelColor: Colors.grey,
                tabs: [
                  Tab(child: Text((isLanguage != true)?"Akun Pembeli":"User Setting", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),),
                  Tab(child: Text((isLanguage != true)?"Akun Toko":"Shop Setting", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),),
                ],
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    new SettingUserActivity(isLanguage, isCurrency),
                    new SettingStoreActivity(isLanguage, isCurrency)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
