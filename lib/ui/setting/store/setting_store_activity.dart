import 'package:ecommerce/ui/setting/store/registration_store_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class SettingStoreActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  SettingStoreActivity(this.isLanguage, this.isCurrency);

  @override
  _SettingStoreActivityState createState() => _SettingStoreActivityState(isLanguage, isCurrency);
}

class _SettingStoreActivityState extends State<SettingStoreActivity> {
  bool isLanguage;
  bool isCurrency;

  _SettingStoreActivityState(this.isLanguage, this.isCurrency);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
                child: Center(
                    child: SvgPicture.asset("assets/image/icon_open_store.svg")
                )
            ),
            SizedBox(height: 40,),
            Row(
              children: [
                SizedBox(width: 40,),
                Icon(Icons.check_circle_outline, color: Color(0xff872934)),
                SizedBox(width: 8,),
                Text((isLanguage != true)?"Buka akun Toko gratis":"open a free shop account", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 14))),
              ],
            ),
            SizedBox(height: 8,),
            Row(
              children: [
                SizedBox(width: 40,),
                Icon(Icons.check_circle_outline, color: Color(0xff872934)),
                SizedBox(width: 8,),
                Text((isLanguage != true)?"Jangkauan luas untuk pembeli":"wide range for buyers", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 14))),
              ],
            ),
            SizedBox(height: 8,),
            Row(
              children: [
                SizedBox(width: 40,),
                Icon(Icons.check_circle_outline, color: Color(0xff872934)),
                SizedBox(width: 8,),
                Text((isLanguage != true)?"Akses ke banyak pelanggan":"access to multiple customers", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 14))),
              ],
            ),
            SizedBox(height: 40,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                  decoration: BoxDecoration(
                    color: Color(0xff872934),
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  child: Material(
                    borderRadius: BorderRadius.circular(6.0),
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: RegistrationStorePage(isLanguage)) );
                      },
                      splashColor: Colors.white,
                      borderRadius: BorderRadius.circular(6.0),
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 22.0, vertical: 8.0),
                          child: Text((isLanguage != true)?"Buka Akun Toko":"open a shop account", style: GoogleFonts.poppins(
                              textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                        ),
                      ),
                    ),
                  )
              ),
            ),
          ],
        ),
      )
    );
  }
}
