import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class RegistrationStorePage extends StatefulWidget {
  bool isLanguage;

  RegistrationStorePage(this.isLanguage);

  @override
  _RegistrationStorePageState createState() => _RegistrationStorePageState(isLanguage);
}

class _RegistrationStorePageState extends State<RegistrationStorePage> {
  bool isLanguage;

  _RegistrationStorePageState(this.isLanguage);

  TextEditingController _editTextNoTelephone = TextEditingController(text: "");
  String dropdownNoCountry = "+62";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 12,),
              Icon(Icons.arrow_back_ios),
              SizedBox(height: 60,),
              Text((isLanguage != true)?"Selamat datang!":"Welcome!", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),)),
              SizedBox(height: 16,),
              Text((isLanguage != true)?"Nomor Telepon":"Phone Number", style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14, color: Colors.grey),)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 4.5,
                    child: DropdownSearch<String>(
                      mode: Mode.MENU,
                      popupBackgroundColor: Color(0xffe3cea3),
                      searchBoxDecoration: InputDecoration(
                        hintText: (isLanguage != true)?"Cari":"Search",
                        contentPadding: EdgeInsets.only(left: 8),
                      ),
                      dropdownSearchDecoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 4),
                      ),
                      showSearchBox: true,
                      showSelectedItem: true,
                      selectedItem: dropdownNoCountry,
                      items: ["+62", "+65", "+60", "+81"],
                      onChanged: (v){
                        setState(() {
                          dropdownNoCountry = v;
                        });
                      },),
                  ),
                  SizedBox(width: 4,),
                  Container(
                    height: MediaQuery.of(context).size.height / 18,
                    width: MediaQuery.of(context).size.width / 1.45 ,
                    child: TextField(
                      controller: _editTextNoTelephone,
                      keyboardType: TextInputType.phone,
                      cursorColor: Colors.black,
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                      decoration: InputDecoration(
                        hintText: (isLanguage != true)?"Masukkan Nomor Telepon":"Enter Phone Number",
                        hintStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                        helperStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black, width: 2),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 40,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                    decoration: BoxDecoration(
                      color: Color(0xff872934),
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: RegistrationStorePage(isLanguage)) );
                        },
                        splashColor: Colors.white,
                        borderRadius: BorderRadius.circular(6.0),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 22.0, vertical: 12.0),
                            child: Text((isLanguage != true)?"Lanjut":"Continue", style: GoogleFonts.poppins(
                                textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                          ),
                        ),
                      ),
                    )
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
