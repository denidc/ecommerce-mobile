import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class VerificationPhonePage extends StatefulWidget {
  bool isLanguage;
  String noTlp;

  VerificationPhonePage(this.isLanguage, this.noTlp);

  @override
  _VerificationPhonePageState createState() => _VerificationPhonePageState(isLanguage, noTlp);
}

class _VerificationPhonePageState extends State<VerificationPhonePage> {
  bool isLanguage;
  String noTlp;

  _VerificationPhonePageState(this.isLanguage, this.noTlp);

  bool isVerify = true;
  final formKey = GlobalKey<FormState>();
  StreamController<ErrorAnimationType> errorController;
  var onTapRecognizer;
  TextEditingController textEditingController = TextEditingController();
  bool hasError = false;
  String currentText = "";

  @override
  void initState() {
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
        Navigator.pop(context);
      };
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    errorController.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Column(
            children: [
              Row(
                children: [
                  IconButton(
                    onPressed: (){},
                    icon: Icon(Icons.close),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width / 4,),
                  Text((isLanguage != true)?"Verifikasi":"Verification", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 18)))
                ],
              ),
              Divider(thickness: 2,),
              SizedBox(height: 20,),
              (isVerify)?Column(
                children: [
                  Text((isLanguage != true)?"Silahkan Melakukan Verifikasi":"Please Verify", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                  Text((isLanguage != true)?"Silahkan verifikasi nomor Anda,":"please verify your number", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 12, color: Colors.grey))),
                  Text((isLanguage != true)?"SMS akan dikirimkan ke nomor yang anda daftarkan":"SMS will be sent to the number you registered", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 12, color: Colors.grey))),
                  SizedBox(height: 20,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(6.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 3,
                              blurRadius: 7,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Material(
                          borderRadius: BorderRadius.circular(6.0),
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                isVerify = !isVerify;
                              });
                              // Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: RegistrationStorePage(isLanguage)) );
                            },
                            splashColor: Colors.white,
                            borderRadius: BorderRadius.circular(6.0),
                            child: Center(
                              child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 20),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset("assets/image/icon_mobile.svg"),
                                      SizedBox(width: 8,),
                                      Text((isLanguage != true)?"SMS ke ":"SMS to", style: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold))),
                                      Text(noTlp, style: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12, color: Colors.grey))),
                                    ],
                                  )
                              ),
                            ),
                          ),
                        )
                    ),
                  ),
                ],
              ):Column(
                children: [
                  SvgPicture.asset("assets/image/icon_mobile.svg"),
                  SizedBox(height: 16,),
                  Text((isLanguage != true)?"Masukkan Kode Verifikasi":"Enter the Verification Code", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                  Text((isLanguage != true)?"Kode verifikasi telah dikirim melalui SMS ke":"The verification code has been sent via SMS to", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 12, color: Colors.grey))),
                  Text(noTlp, style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 12, color: Colors.grey))),
                  SizedBox(height: 20,),
                  Form(
                    key: formKey,
                    child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 80),
                        child: PinCodeTextField(
                          appContext: context,
                          pastedTextStyle: TextStyle(
                            color: Colors.transparent,
                            fontWeight: FontWeight.bold,
                          ),
                          length: 4,
                          obscureText: false,
                          animationType: AnimationType.fade,
                          backgroundColor: Colors.transparent,
                          validator: (v) {
                            if (v.length < 3) {
                              return "I'm from validator";
                            } else {
                              return null;
                            }
                          },
                          pinTheme: PinTheme(
                              shape: PinCodeFieldShape.underline,
                              borderRadius: BorderRadius.circular(5),
                              fieldHeight: 50,
                              fieldWidth: 40,
                              selectedColor: Color(0xff872934),
                              activeColor: Color(0xff872934),
                              disabledColor: Color(0xff872934),
                              inactiveColor: Color(0xff872934)
                          ),
                          animationDuration: Duration(milliseconds: 300),
                          errorAnimationController: errorController,
                          controller: textEditingController,
                          keyboardType: TextInputType.number,
                          onCompleted: (v) {
                            print("Completed" + v);
                          },
                          onChanged: (value) {
                            print(value);
                            setState(() {
                              currentText = value;
                            });
                          },
                          beforeTextPaste: (text) {
                            print("Allowing to paste $text");
                            //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                            //but you can show anything you want here, like your pop up saying wrong paste format or etc
                            return true;
                          },
                        )),
                  ),
                  SizedBox(height: 16,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 80),
                    child: Container(
                        decoration: BoxDecoration(
                          color: Color(0xff872934),
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        child: Material(
                          borderRadius: BorderRadius.circular(6.0),
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                isVerify = !isVerify;
                              });
                              // Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: RegistrationStorePage(isLanguage)) );
                            },
                            splashColor: Colors.white,
                            borderRadius: BorderRadius.circular(6.0),
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 20),
                                child: Text((isLanguage != true)?"Verifikasi":"Verify", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white))),
                              ),
                            ),
                          ),
                        )
                    ),
                  ),
                  SizedBox(height: 18,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text((isLanguage != true)?"Tidak menerima kode?":"Didn't receive the code?", style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey))),
                      Text((isLanguage != true)?" Kirim Ulang ":" Resending ", style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Color(0xff872934)))),
                      Text((isLanguage != true)?"Kode Anda":"Your Code", style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey))),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
