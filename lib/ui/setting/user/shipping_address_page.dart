import 'package:dropdown_search/dropdown_search.dart';
import 'package:ecommerce/utils/google_maps/search_address_maps.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class ShippingAddressPage extends StatefulWidget {
  bool isLanguage;
  Position currentPosition;

  ShippingAddressPage(this.isLanguage, this.currentPosition);

  @override
  _ShippingAddressPageState createState() => _ShippingAddressPageState(isLanguage, currentPosition);
}

class _ShippingAddressPageState extends State<ShippingAddressPage> {
  bool isLanguage;
  Position currentPosition;

  _ShippingAddressPageState(this.isLanguage, this.currentPosition);

  String dropdownCountry = "";
  String dropdownProvince = "";
  String dropdownDistrict = "";
  String dropdownSubDistrict = "";
  String dropdownNoCountry = "+62";

  TextEditingController _editTextReceiverName = TextEditingController(text: "");
  TextEditingController _editTextNoTelephone = TextEditingController(text: "");
  TextEditingController _editTextNoPostalCode = TextEditingController(text: "");
  TextEditingController _editTextShippingAddress = TextEditingController(text: "");

  @override
  void initState() {
    Future.delayed(Duration.zero, ()async{
      var addresses = await Geocoder.local.findAddressesFromCoordinates(new Coordinates(currentPosition.latitude, currentPosition.longitude));
      var first = addresses.first;
      List<Placemark> placemark = await Geolocator().placemarkFromCoordinates(currentPosition.latitude, currentPosition.longitude);
      setState(() {
        dropdownCountry = first.countryName;
        dropdownProvince = first.adminArea;
        dropdownDistrict = first.subAdminArea;
        dropdownSubDistrict = first.locality;
        _editTextShippingAddress.text = first.addressLine;
        _editTextNoPostalCode.text = first.postalCode;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 18),
          child: Text((isLanguage != true)?"Alamat Pengiriman":"Shipping Address", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){},
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      bottomNavigationBar: Container(
        height: MediaQuery.of(context).size.height / 16,
        decoration: BoxDecoration(
            color: Colors.black
        ),
        child: Center(
          child: Text((isLanguage != true)?"Simpan":"Save", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 18))),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 24,),
              Text((isLanguage != true)?"Alamat Baru":"New Address", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),)),
              SizedBox(height: 16,),
              Divider(),
              Row(
                children: [
                  Text((isLanguage != true)?"Nama Penerima":"Recipient's name", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
                  Text("*", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 16),))
                ],
              ),
              Container(
                height: MediaQuery.of(context).size.height / 18,
                child: TextField(
                  controller: _editTextReceiverName,
                  keyboardType: TextInputType.name,
                  cursorColor: Colors.black,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                  decoration: InputDecoration(
                      hintText: (isLanguage != true)?"Masukkan Nama Penerima":"Enter Recipient's name",
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                      helperStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 2),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 2),
                      ),
                  ),
                ),
              ),
              SizedBox(height: 12,),
              Row(
                children: [
                  Text((isLanguage != true)?"Negara":"Country", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
                  Text("*", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 16),))
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: DropdownSearch<String>(
                  mode: Mode.MENU,
                  popupBackgroundColor: Color(0xffe3cea3),
                  searchBoxDecoration: InputDecoration(
                    hintText: (isLanguage != true)?"Search":"Cari",
                    contentPadding: EdgeInsets.only(left: 8),
                  ),
                  dropdownSearchDecoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 4),
                  ),
                  showSearchBox: true,
                  showSelectedItem: true,
                  selectedItem: dropdownCountry,
                  items: ["Indonesia", "Singapura", "Malaysia", "Jepang"],
                  onChanged: (v){
                    setState(() {
                      dropdownCountry = v;
                    });
                  },),
              ),
              SizedBox(height: 12,),
              Row(
                children: [
                  Text((isLanguage != true)?"Nomor Telepon":"Phone Number", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
                  Text("*", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 16),))
                ],
              ),
              Row(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 4.5,
                    child: DropdownSearch<String>(
                      mode: Mode.MENU,
                      popupBackgroundColor: Color(0xffe3cea3),
                      searchBoxDecoration: InputDecoration(
                        hintText: (isLanguage != true)?"Cari":"Search",
                        contentPadding: EdgeInsets.only(left: 8),
                      ),
                      dropdownSearchDecoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 4),
                      ),
                      showSearchBox: true,
                      showSelectedItem: true,
                      selectedItem: dropdownNoCountry,
                      items: ["+62", "+65", "+60", "+81"],
                      onChanged: (v){
                        setState(() {
                          dropdownNoCountry = v;
                        });
                      },),
                  ),
                  SizedBox(width: 4,),
                  Container(
                    height: MediaQuery.of(context).size.height / 18,
                    width: MediaQuery.of(context).size.width / 1.45 ,
                    child: TextField(
                      controller: _editTextNoTelephone,
                      keyboardType: TextInputType.phone,
                      cursorColor: Colors.black,
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                      decoration: InputDecoration(
                        hintText: (isLanguage != true)?"Masukkan Nomor Telepon":"Enter Phone Number",
                        hintStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                        helperStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black, width: 2),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 12,),
              Text((isLanguage != true)?"Alamat Pengiriman":"Shipping Address", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
              Container(
                height: MediaQuery.of(context).size.height / 18,
                child: TextField(
                  controller: _editTextShippingAddress,
                  keyboardType: TextInputType.streetAddress,
                  cursorColor: Colors.black,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                  decoration: InputDecoration(
                    hintText: (isLanguage != true)?"Nama Jalan, Nomor Rumah, RT/RW":"Street Name",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    helperStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 12,),
              Row(
                children: [
                  Text((isLanguage != true)?"Provinsi":"Province", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
                  Text("*", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 16),))
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: DropdownSearch<String>(
                  mode: Mode.MENU,
                  popupBackgroundColor: Color(0xffe3cea3),
                  searchBoxDecoration: InputDecoration(
                    hintText: (isLanguage != true)?"Cari":"Search",
                    contentPadding: EdgeInsets.only(left: 8),
                  ),
                  dropdownSearchDecoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 4),
                  ),
                  showSearchBox: true,
                  showSelectedItem: true,
                  selectedItem: dropdownProvince,
                  items: ["Jawa Timur", "Jawa Tengah", "Jawa Barat", "Aceh"],
                  onChanged: (v){
                    setState(() {
                      dropdownProvince = v;
                    });
                  },),
              ),
              SizedBox(height: 12,),
              Row(
                children: [
                  Text((isLanguage != true)?"Kabupaten":"District", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
                  Text("*", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 16),))
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: DropdownSearch<String>(
                  mode: Mode.MENU,
                  popupBackgroundColor: Color(0xffe3cea3),
                  searchBoxDecoration: InputDecoration(
                    hintText: (isLanguage != true)?"Cari":"Search",
                    contentPadding: EdgeInsets.only(left: 8),
                  ),
                  dropdownSearchDecoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 4),
                  ),
                  showSearchBox: true,
                  showSelectedItem: true,
                  selectedItem: dropdownDistrict,
                  items: ["Kabupaten Sidoarjo", "Surabaya", "Mojokerto", "Malang"],
                  onChanged: (v){
                    setState(() {
                      dropdownDistrict = v;
                    });
                  },),
              ),
              SizedBox(height: 12,),
              Row(
                children: [
                  Text((isLanguage != true)?"Kecamatan":"Sub District", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
                  Text("*", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 16),))
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: DropdownSearch<String>(
                  mode: Mode.MENU,
                  popupBackgroundColor: Color(0xffe3cea3),
                  searchBoxDecoration: InputDecoration(
                    hintText: (isLanguage != true)?"Cari":"Search",
                    contentPadding: EdgeInsets.only(left: 8),
                  ),
                  dropdownSearchDecoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 4),
                  ),
                  showSearchBox: true,
                  showSelectedItem: true,
                  selectedItem: dropdownSubDistrict,
                  items: ["Kecamatan Sukodono", "Wage", "Gedangan", "Waru"],
                  onChanged: (v){
                    setState(() {
                      dropdownSubDistrict = v;
                    });
                  },),
              ),
              SizedBox(height: 12,),
              Row(
                children: [
                  Text((isLanguage != true)?"Kode Pos":"Postal Code", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
                  Text("*", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 16),))
                ],
              ),
              Container(
                height: MediaQuery.of(context).size.height / 18,
                child: TextField(
                  controller: _editTextNoPostalCode,
                  keyboardType: TextInputType.number,
                  cursorColor: Colors.black,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                  decoration: InputDecoration(
                    hintText: (isLanguage != true)?"Masukkan Kode Pos":"Enter Postal Code",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    helperStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16,),
              Container(
                  decoration: BoxDecoration(
                    color: Color(0xffe3cea3),
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  child: Material(
                    borderRadius: BorderRadius.circular(6.0),
                    color: Colors.transparent,
                    child: AnimatedSwitcher(
                        duration: Duration.zero,
                        child: InkWell(
                          onTap: () async{
                            Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: SearchAddressMaps(isLanguage, currentPosition)) );
                          },
                          splashColor: Colors.white,
                          borderRadius: BorderRadius.circular(6.0),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 22.0, vertical: 4.0),
                              child: Text((isLanguage != true)?"Atur alamat lewat map":"set the address via maps", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold)),),
                            ),
                          ),
                        )
                    ),
                  )
              ),
              SizedBox(height: 16,),
            ],
          ),
        ),
      ),
    );
  }
}
