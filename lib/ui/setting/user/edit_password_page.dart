import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class EditPasswordPage extends StatefulWidget {
  bool isLanguage;

  EditPasswordPage(this.isLanguage);

  @override
  _EditPasswordPageState createState() => _EditPasswordPageState(isLanguage);
}

class _EditPasswordPageState extends State<EditPasswordPage> {
  bool isLanguage;

  _EditPasswordPageState(this.isLanguage);

  TextEditingController _editTextPasswordOld = TextEditingController(text: "");
  TextEditingController _editTextPasswordNew = TextEditingController(text: "");
  TextEditingController _editTextPasswordNewConfirm = TextEditingController(text: "");

  FocusNode _nodePasswordOld;
  FocusNode _nodePasswordNew;
  FocusNode _nodePasswordNewConfirm;

  bool _obscurePasswordOld = true;
  bool _obscurePasswordNew = true;
  bool _obscurePasswordNewConfirm = true;

  void _togglePasswordOld() {
    setState(() {
      _obscurePasswordOld = !_obscurePasswordOld;
    });
  }
  void _togglePasswordNew() {
    setState(() {
      _obscurePasswordNew = !_obscurePasswordNew;
    });
  }
  void _togglePasswordNewConfirm() {
    setState(() {
      _obscurePasswordNewConfirm = !_obscurePasswordNewConfirm;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 10),
          child: Text((isLanguage != true)?"Ubah Kata Sandi":"Edit Password", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){},
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 24,),
              Text((isLanguage != true)?"Ubah Kata Sandi":"Edit Password", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),)),
              SizedBox(height: 8,),
              Divider(),
              Text((isLanguage != true)?"Kata Sandi Lama":"Old Password", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
              Container(
                height: MediaQuery.of(context).size.height / 18,
                child: TextField(
                  enableInteractiveSelection: false,
                  autocorrect: false,
                  focusNode: _nodePasswordOld,
                  onSubmitted: (term){
                    _nodePasswordOld.unfocus();
                  },
                  obscureText: _obscurePasswordOld,
                  controller: _editTextPasswordOld,
                  keyboardType: TextInputType.visiblePassword,
                  cursorColor: Colors.black,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                  decoration: InputDecoration(
                    hintText: (isLanguage != true)?"Masukkan Kata Sandi lama":"Enter Old Password",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    helperStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2),
                    ),
                      suffixIcon: IconButton(
                        onPressed: _togglePasswordOld,
                        icon: Icon(_obscurePasswordOld ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off,
                          color: Colors.black),
                      )
                  ),
                ),
              ),
              SizedBox(height: 16,),
              Text((isLanguage != true)?"Kata Sandi Baru":"New Password", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
              Container(
                height: MediaQuery.of(context).size.height / 18,
                child: TextField(
                  enableInteractiveSelection: false,
                  autocorrect: false,
                  focusNode: _nodePasswordNew,
                  onSubmitted: (term){
                    _nodePasswordNew.unfocus();
                  },
                  obscureText: _obscurePasswordNew,
                  controller: _editTextPasswordNew,
                  keyboardType: TextInputType.visiblePassword,
                  cursorColor: Colors.black,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                  decoration: InputDecoration(
                    hintText: (isLanguage != true)?"Masukkan Kata Sandi baru":"Enter New Password",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    helperStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2),
                    ),
                      suffixIcon: IconButton(
                        onPressed: _togglePasswordNew,
                        icon: Icon(_obscurePasswordNew ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off,
                            color: Colors.black),
                      )
                  ),
                ),
              ),
              SizedBox(height: 16,),
              Text((isLanguage != true)?"Konfirmasi Kata Sandi Baru":"Confirmation New Password", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
              Container(
                height: MediaQuery.of(context).size.height / 18,
                child: TextField(
                  enableInteractiveSelection: false,
                  autocorrect: false,
                  focusNode: _nodePasswordNewConfirm,
                  onSubmitted: (term){
                    _nodePasswordNewConfirm.unfocus();
                  },
                  obscureText: _obscurePasswordNewConfirm,
                  controller: _editTextPasswordNewConfirm,
                  keyboardType: TextInputType.visiblePassword,
                  cursorColor: Colors.black,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14, color: Colors.black)),
                  decoration: InputDecoration(
                    hintText: "Masukkan Konfirmasi Password baru",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    helperStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black, width: 2),
                    ),
                      suffixIcon: IconButton(
                        onPressed: _togglePasswordNewConfirm,
                        icon: Icon(_obscurePasswordNewConfirm ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off,
                            color: Colors.black),
                      )
                  ),
                ),
              ),
              SizedBox(height: 32,),
              Container(
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  child: Material(
                    borderRadius: BorderRadius.circular(6.0),
                    color: Colors.transparent,
                    child: AnimatedSwitcher(
                        duration: Duration.zero,
                        child: InkWell(
                          onTap: () async{
//                            Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: MainActivity()) );
                          },
                          splashColor: Colors.white,
                          borderRadius: BorderRadius.circular(6.0),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 22.0, vertical: 4.0),
                              child: Text((isLanguage != true)?"Simpan":"Save", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                            ),
                          ),
                        )
                    ),
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}
