import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PointsPage extends StatefulWidget {
  bool isLanguage;

  PointsPage(this.isLanguage);

  @override
  _PointsPageState createState() => _PointsPageState(isLanguage);
}

class _PointsPageState extends State<PointsPage> {
  bool isLanguage;

  _PointsPageState(this.isLanguage);

  ScrollController _scrollController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 5),
          child: Text((isLanguage != true)?"Poin":"Points", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){},
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 24,),
                  Text((isLanguage != true)?"Poin Saya":"My Point", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)),
                  SizedBox(height: 8,),
                  Divider(thickness: 1,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text((isLanguage != true)?"Poin Aktif":"Active Point", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 14),)),
                      Text("500 P", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14),)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text((isLanguage != true)?"Poin Digunakan":"Points Used", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 14),)),
                      Text("0 P", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14),)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text((isLanguage != true)?"Poin Kadaluarsa":"Points Expire", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 14),)),
                      Text("0 P", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 14),)),
                    ],
                  ),
                  Text((isLanguage != true)?"Point kadaluarsa sebelum 30 hari":"Points expire before 30 days", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 8),)),
                  SizedBox(height: 16,)
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.black
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                child: Text((isLanguage != true)?"Poin Aktif":"Active Point", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),)),
              ),
            ),
            SizedBox(height: 8,),
            ListView.builder(
                controller: _scrollController,
                shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: 5,
                itemBuilder: (context, index){
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 14,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("03. 08. 2020", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 12),)),
                              Text((isLanguage != true)?"Poin Hadiah registrasi":"Registration Prize Points", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 14),)),
                              Text((isLanguage != true)?"Kadaluarsa sebelum tanggal 03. 09. 2020":"Expires before date 03. 09. 2020", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 8),)),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("+100 P", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12),))
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                }
            ),
          ],
        ),
      ),
    );
  }
}
