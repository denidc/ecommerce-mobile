import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class ProfilePage extends StatefulWidget {
  bool isLanguage;

  ProfilePage(this.isLanguage);

  @override
  _ProfilePageState createState() => _ProfilePageState(isLanguage);
}

class _ProfilePageState extends State<ProfilePage> {
  bool isLanguage;

  _ProfilePageState(this.isLanguage);

  TextEditingController _profileTextUsername = TextEditingController(text: "Sarah Razzer");
  TextEditingController _profileTextTelephone = TextEditingController(text: "082xxxxxxxxx");
  TextEditingController _profileTextEmail = TextEditingController(text: "sarahrazzer@gmail.com");

  String dropdownGender;

  DateTime selectedDate = DateTime.now();

  @override
  void initState() {
    setState(() {
      dropdownGender= (isLanguage != true)?"Perempuan":"Female";
    });
    super.initState();
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1980, 1),
        lastDate: DateTime(int.parse(selectedDate.toLocal().toString().split('-')[0]), 13),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: const Color(0xffe3cea3),
            accentColor: const Color(0xffe3cea3),
            colorScheme: ColorScheme.light(primary: const Color(0xffe3cea3)),
            buttonTheme: ButtonThemeData(
                textTheme: ButtonTextTheme.primary
            ),
          ),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 10),
          child: Text("Informasi Akun", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){},
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 24,),
              Text((isLanguage != true)?"Profil":"Profile", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),)),
              SizedBox(height: 16,),
              Divider(),
              Container(
                height: MediaQuery.of(context).size.height / 18,
                child: Directionality(
                  textDirection: TextDirection.rtl,
                  child: TextField(
                    controller: _profileTextUsername,
                    textAlign: TextAlign.right,
                    cursorColor: Colors.black,
                    keyboardType: TextInputType.name,
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),
                    decoration: InputDecoration(
                      suffixIconConstraints: BoxConstraints(
                          minHeight: 0
                      ),
                      suffixIcon: Text((isLanguage != true)?"Nama Pengguna":"Username", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),),
                      hintText: (isLanguage != true)?"Masukkan Nama Pengguna":"Enter Username",
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                      helperStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent, width: 0),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent, width: 0),
                      ),
                    ),
                  ),
                ),
              ),
              Divider(),
              Container(
                height: MediaQuery.of(context).size.height / 18,
                child: Directionality(
                  textDirection: TextDirection.rtl,
                  child: TextField(
                    controller: _profileTextTelephone,
                    textAlign: TextAlign.right,
                    cursorColor: Colors.black,
                    keyboardType: TextInputType.phone,
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),
                    decoration: InputDecoration(
                      suffixIconConstraints: BoxConstraints(
                          minHeight: 0
                      ),
                      suffixIcon: Text((isLanguage != true)?"Nomor Telepon":"Phone Number", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),),
                      hintText: (isLanguage != true)?"Masukkan Nomor Telepon":"Enter Phone Number",
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                      helperStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent, width: 0),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent, width: 0),
                      ),
                    ),
                  ),
                ),
              ),
              Divider(),
              Container(
                height: MediaQuery.of(context).size.height / 18,
                child: Directionality(
                  textDirection: TextDirection.rtl,
                  child: TextField(
                    controller: _profileTextEmail,
                    textAlign: TextAlign.right,
                    cursorColor: Colors.black,
                    keyboardType: TextInputType.emailAddress,
                    style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),
                    decoration: InputDecoration(
                      suffixIconConstraints: BoxConstraints(
                        minHeight: 0
                      ),
                      suffixIcon: Text("Email", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),),
                      hintText: (isLanguage != true)?"Masukkan Email":"Enter Email",
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                      helperStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent, width: 0),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent, width: 0),
                      ),
                    ),
                  ),
                ),
              ),
              Divider(),
              GestureDetector(
                onTap: () => _selectDate(context),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.transparent
                  ),
                  child: Column(
                    children: [
                      SizedBox(height: 12,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text((isLanguage != true)?"Tanggal Lahir":"Date of Birth", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                          Text(selectedDate.toLocal().toString().split(' ')[0], style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                        ],
                      ),
                      SizedBox(height: 12,),
                    ],
                  ),
                ),
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text((isLanguage != true)?"Jenis Kelamin":"Gender", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                  Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 2.6,
                        child: DropdownSearch<String>(
                          mode: Mode.MENU,
                          popupBackgroundColor: Color(0xffe3cea3),
                          dropdownSearchDecoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 8),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent, width: 0),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent, width: 0),
                            ),
                          ),
                          showSelectedItem: true,
                          selectedItem: dropdownGender,
                          items: [(isLanguage != true)?"Laki-Laki":"Male", (isLanguage != true)?"Perempuan":"Female", (isLanguage != true)?"Lainnya":"Etc"],
                          onChanged: (v){
                            setState(() {
                              dropdownGender = v;
                            });
                          },),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 18,),
              Container(
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  child: Material(
                    borderRadius: BorderRadius.circular(6.0),
                    color: Colors.transparent,
                    child: AnimatedSwitcher(
                        duration: Duration.zero,
                        child: InkWell(
                          onTap: () async{
//                            Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: MainActivity()) );
                          },
                          splashColor: Colors.white,
                          borderRadius: BorderRadius.circular(6.0),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 22.0, vertical: 4.0),
                              child: Text((isLanguage != true)?"Simpan":"Save", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                            ),
                          ),
                        )
                    ),
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}
