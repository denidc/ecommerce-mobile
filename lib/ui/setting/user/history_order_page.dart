import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HistoryOrderPage extends StatefulWidget {
  bool isLanguage;

  HistoryOrderPage(this.isLanguage);

  @override
  _HistoryOrderPageState createState() => _HistoryOrderPageState(isLanguage);
}

class _HistoryOrderPageState extends State<HistoryOrderPage> {
  bool isLanguage;

  _HistoryOrderPageState(this.isLanguage);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 24),
          child: Text((isLanguage != true)?"Riwayat Pesanan":"Order History", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){},
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 24,),
              Text((isLanguage != true)?"Riwayat Pesanan":"Order History", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)),
              SizedBox(height: 8,),
              Divider(thickness: 1,),

            ],
          ),
        ),
      ),
    );
  }
}
