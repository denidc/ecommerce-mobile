import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class CountrySettingPage extends StatefulWidget {
  bool isLanguage;

  CountrySettingPage(this.isLanguage);

  @override
  _CountrySettingPageState createState() => _CountrySettingPageState(isLanguage);
}

class _CountrySettingPageState extends State<CountrySettingPage> {
  bool isLanguage;

  _CountrySettingPageState(this.isLanguage);

  List<RadioModel> sampleData = new List<RadioModel>();
  ScrollController _scrollController;
  int _indexRadio;

  @override
  void initState() {
    sampleData.add(new RadioModel(0,false, 'THAILAND'));
    sampleData.add(new RadioModel(1,false, 'PHILIPPINES'));
    sampleData.add(new RadioModel(2,false, 'SINGAPORE'));
    sampleData.add(new RadioModel(3,false, 'MALAYSIA'));
    sampleData.add(new RadioModel(4,true, 'INDONESIA'));
    sampleData.add(new RadioModel(5,false, 'VIETNAM'));
    sampleData.add(new RadioModel(6,false, 'TAIWAN'));
    sampleData.add(new RadioModel(7,false, 'CHINA'));
    sampleData.add(new RadioModel(8,false, 'UNITED STATES'));
    sampleData.add(new RadioModel(9,false, 'CANADA'));
    sampleData.add(new RadioModel(10,false, 'NEW ZEALAND'));
    sampleData.add(new RadioModel(11,false, 'AUSTRALIA'));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 24),
          child: Text((isLanguage != true)?"Pengaturan Negara":"Country Setting", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){},
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      bottomNavigationBar: Container(
        height: MediaQuery.of(context).size.height / 16,
        decoration: BoxDecoration(
            color: Colors.black
        ),
        child: Center(
          child: Text((isLanguage != true)?"Simpan":"Save", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 18))),
        ),
      ),
      body: new Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: new SingleChildScrollView(
          controller: _scrollController,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              new SizedBox(height: 24,),
              new Text((isLanguage != true)?"Pengaturan Negara":"Country Setting", style: GoogleFonts.poppins(
                  textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),
              new SizedBox(height: 8,),
              new ListView.builder(
                controller: _scrollController,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: sampleData.length,
                itemBuilder: (BuildContext context, int index) {
                  return new InkWell(
                    highlightColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    onTap: () {
                      setState(() {
                        sampleData.forEach((element) => element.isSelected = false);
                        sampleData[index].isSelected = true;

                      });
                    },
                    child: new RadioItem(sampleData[index]),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          new Divider(),
          new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Container(
                margin: new EdgeInsets.only(top: _item.isSelected?0:2, bottom: _item.isSelected?0:2),
                child: new Text(_item.text, style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontWeight: FontWeight.bold, color: _item.isSelected?Colors.black:Colors.grey, fontSize: 14))),
              ),
              _item.isSelected ? new Padding(
                padding: const EdgeInsets.only(right: 8),
                child: new Icon(Icons.check_box),
              ) : new Container(),
            ],
          ),
        ],
      ),
    );
  }
}

class RadioModel {
  int id;
  bool isSelected;
  final String text;

  RadioModel(this.id, this.isSelected, this.text);
}
