import 'package:ecommerce/ui/setting/user/cart_shop_page.dart';
import 'package:ecommerce/ui/setting/user/country_setting_page.dart';
import 'package:ecommerce/ui/setting/user/edit_password_page.dart';
import 'package:ecommerce/ui/setting/user/history_order_page.dart';
import 'package:ecommerce/ui/setting/user/points_page.dart';
import 'package:ecommerce/ui/setting/user/profile_page.dart';
import 'package:ecommerce/ui/setting/user/shipping_address_page.dart';
import 'package:ecommerce/ui/splash_screen/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingUserActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  SettingUserActivity(this.isLanguage, this.isCurrency);

  @override
  _SettingUserActivityState createState() => _SettingUserActivityState(isLanguage, isCurrency);
}

class _SettingUserActivityState extends State<SettingUserActivity> {
  bool isLanguage;
  bool isCurrency;

  _SettingUserActivityState(this.isLanguage, this.isCurrency);

  Position _currentPosition;
  @override
  void initState() {
    Future.delayed(Duration.zero, ()async{
      _currentPosition = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
      setState((){
        print(_currentPosition);
      });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 16,),
                Row(
                  children: [
                    CircleAvatar(
                      backgroundImage: AssetImage("assets/image/seli_marchelia.png"),
                      radius: 20,
                    ),
                    SizedBox(width: 8,),
                    Text("Sarah Razzer", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                    SizedBox(width: 4,),
                    Text("("),
                    Text("sarahrazzer@gmail.com", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontSize: 12),)),
                    Text(")")
                  ],
                ),
                SizedBox(height: 18,),
                Text((isLanguage != true)?"Pengaturan Akun Saya":"Setting My Account", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontSize: 14),)),
                Divider(),
                SizedBox(height: 4,),
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ProfilePage(isLanguage)) );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text((isLanguage != true)?"Informasi Akun":"Account Information", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                      Icon(Icons.chevron_right)
                    ],
                  ),
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Notifikasi":"Notification", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                    Icon(Icons.chevron_right)
                  ],
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: EditPasswordPage(isLanguage)) );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text((isLanguage != true)?"Ubah Kata Sandi":"Change Password", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                      Icon(Icons.chevron_right)
                    ],
                  ),
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Media Sosial":"Social Media", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                    Icon(Icons.chevron_right)
                  ],
                ),
                SizedBox(height: 24,),
                //information shop
                Text((isLanguage != true)?"Informasi Belanja":"Shopping Information", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontSize: 14),)),
                Divider(),
                SizedBox(height: 4,),
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: CartShopPage(isLanguage)) );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text((isLanguage != true)?"Keranjang Belanja":"Shopping Cart", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                      Icon(Icons.chevron_right)
                    ],
                  ),
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ShippingAddressPage(isLanguage, _currentPosition)) );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text((isLanguage != true)?"Alamat Pengiriman":"Shipping Address", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                      Icon(Icons.chevron_right)
                    ],
                  ),
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: HistoryOrderPage(isLanguage)) );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text((isLanguage != true)?"Riwayat Pesanan":"Order History", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                      Icon(Icons.chevron_right)
                    ],
                  ),
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: PointsPage(isLanguage)) );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text((isLanguage != true)?"Poin":"Points", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                      Icon(Icons.chevron_right)
                    ],
                  ),
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Ulasan":"Review", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                    Icon(Icons.chevron_right)
                  ],
                ),
                SizedBox(height: 18,),
                //information shop
                Text((isLanguage != true)?"Pengaturan":"Setting", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontSize: 14),)),
                Divider(),
                SizedBox(height: 4,),
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: CountrySettingPage(isLanguage)) );
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Colors.transparent
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text((isLanguage != true)?"Negara":"Country", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                        Row(
                          children: [
                            Row(
                              children: [
                                Text("INDONESIA", style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14),)),
                                Icon(Icons.chevron_right)
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                GestureDetector(
                  onTap: (){
                    showModalBottomSheet(
                        context: context,
                        builder: (context){
                          return Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              GestureDetector(
                                onTap: ()async{
                                  SharedPreferences pref = await SharedPreferences.getInstance();
                                  pref.setBool("is_language", false);
                                  Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: SplashScreen()) );
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: Colors.transparent
                                  ),
                                  child: Column(
                                    children: [
                                      SizedBox(height: 16,),
                                      Text("Indonesia", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                                    ],
                                  ),
                                ),
                              ),
                              Divider(),
                              GestureDetector(
                                onTap: ()async{
                                  SharedPreferences pref = await SharedPreferences.getInstance();
                                  pref.setBool("is_language", true);
                                  Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: SplashScreen()) );
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: Colors.transparent
                                  ),
                                  child: Column(
                                    children: [
                                      Text("English", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                                      SizedBox(height: 16,),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          );
                        }
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.transparent
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text((isLanguage != true)?"Bahasa":"Language", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                        Row(
                          children: [
                            Text((isLanguage != true)?"Indonesia":"English", style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14),)),
                            Icon(Icons.chevron_right)
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                GestureDetector(
                  onTap: (){
                    showModalBottomSheet(
                        context: context,
                        builder: (context){
                          return Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              GestureDetector(
                                onTap: ()async{
                                  SharedPreferences pref = await SharedPreferences.getInstance();
                                  pref.setBool("is_currency", false);
                                  Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: SplashScreen()) );
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: Colors.transparent
                                  ),
                                  child: Column(
                                    children: [
                                      SizedBox(height: 16,),
                                      Text("Rp(IDR)", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                                    ],
                                  ),
                                ),
                              ),
                              Divider(),
                              GestureDetector(
                                onTap: ()async{
                                  SharedPreferences pref = await SharedPreferences.getInstance();
                                  pref.setBool("is_currency", true);
                                  Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: SplashScreen()) );
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: Colors.transparent
                                  ),
                                  child: Column(
                                    children: [
                                      Text(r"$(USD)", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                                      SizedBox(height: 16,),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          );
                        }
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.transparent
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text((isLanguage != true)?"Mata Uang":"Currency", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                        Row(
                          children: [
                            Text((isCurrency != true)?"Rp(IDR)":r"$(USD)", style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14)),),
                            Icon(Icons.chevron_right),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Privasi":"Privacy", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                    Icon(Icons.chevron_right)
                  ],
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Syarat & Ketentuan":"Term & Provisions", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                    Icon(Icons.chevron_right)
                  ],
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Kebijakan Privasi":"Privacy Policy", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),)),
                    Icon(Icons.chevron_right)
                  ],
                ),
                SizedBox(height: 4,),
                Divider(),
                SizedBox(height: 4,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(Icons.arrow_back),
                    SizedBox(width: 8,),
                    Text((isLanguage != true)?"Keluar":"Exit", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),))
                  ],
                ),
                SizedBox(height: 20,),
                Text((isLanguage != true)?"Versi 1.0.0":"Version 1.0.0", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontSize: 14),))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
