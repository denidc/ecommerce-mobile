import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CartShopPage extends StatefulWidget {
  bool isLanguage;

  CartShopPage(this.isLanguage);

  @override
  _CartShopPageState createState() => _CartShopPageState(isLanguage);
}

class _CartShopPageState extends State<CartShopPage> {
  bool isLanguage;

  _CartShopPageState(this.isLanguage);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 24),
          child: Text((isLanguage != true)?"Keranjang Belanja":"Shopping Cart", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){},
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      bottomNavigationBar: Container(
        height: MediaQuery.of(context).size.height / 16,
        decoration: BoxDecoration(
          color: Colors.black
        ),
        child: Center(
          child: Text((isLanguage != true)?"Memesan":"Booking", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 18))),
        ),
      ),
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.black
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                Text((isLanguage != true)?"Keranjang Belanja":"Shopping Cart", style: GoogleFonts.poppins(
                  textStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 12))),
                Text(">", style: GoogleFonts.poppins(
                  textStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 12))),
                  Text((isLanguage != true)?"Pesanan":"Order", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 12))),
                  Text(">", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 12))),
                  Text((isLanguage != true)?"Pembayaran":"Payment", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 12))),
                ],
              ),
            ),
          ),

        ],
      ),
    );
  }
}
