import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce/ui/chat/chat_seller_activity.dart';
import 'package:ecommerce/utils/custom_card.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';

class ProductDetailActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  ProductDetailActivity(this.isLanguage, this.isCurrency);

  @override
  _ProductDetailActivityState createState() => _ProductDetailActivityState(isLanguage, isCurrency);
}

class _ProductDetailActivityState extends State<ProductDetailActivity> with SingleTickerProviderStateMixin{
  bool isLanguage;
  bool isCurrency;

  _ProductDetailActivityState(this.isLanguage, this.isCurrency);

  TabController _tabController;
  ScrollController _scrollViewController;

  int _currentHeader = 0;

  List carouselHeader = [
    "assets/image/parfume_sakura.png",
    "assets/image/parfume_sakura.png",
    "assets/image/parfume_sakura.png",
    "assets/image/parfume_sakura.png",
  ];

  List<T> mapHeader<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: 3,
      vsync: this,
    );
    _scrollViewController =ScrollController();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
    _scrollViewController.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 8),
          child: Text((isLanguage != true)?"Produk Detail":"Detail Product", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){},
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      body: DefaultTabController(
        length: _tabController.length,
        child: NestedScrollView(
          controller: _scrollViewController,
          headerSliverBuilder:
              (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor: Colors.white,
                flexibleSpace: FlexibleSpaceBar(
                  background: Column(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: Stack(
                          children: [
                            CarouselSlider(
                              options: CarouselOptions(
                                  autoPlay: false,
                                  height: MediaQuery.of(context).size.height / 4,
                                  scrollDirection: Axis.horizontal,
                                  enableInfiniteScroll: true,
                                  onPageChanged: (index, reason){
                                    setState(() {
                                      _currentHeader = index;
                                    });
                                  }
                              ),
                              items: carouselHeader.map((e) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(e),
                                        fit: BoxFit.fill,
                                      )
                                  ),
                                );
                              }).toList(),
                            ),
                            Positioned(
                              left: MediaQuery.of(context).size.width / 2.2,
                              bottom: 4,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: mapHeader<Widget>(carouselHeader, (index, url) {
                                  return Container(
                                    width: _currentHeader == index ? 10 : 8,
                                    height: _currentHeader == index ? 10 : 8,
                                    margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: _currentHeader == index ? Color(0xff872934) : Colors.white,
                                    ),
                                  );
                                }),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("LIBERTY LIP", style: GoogleFonts.poppins(
                                textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey))),
                            Text("[LIPS] Liberty Fluid Matte Lip Paint", style: GoogleFonts.poppins(
                                textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,))),
                            Container(
                              height: MediaQuery.of(context).size.height / 32,
                              width: MediaQuery.of(context).size.width / 2.8,
                              decoration: BoxDecoration(
                                  color: Color(0xff872934),
                                  borderRadius: BorderRadius.circular(4)
                              ),
                              child: Center(
                                child: Text((isLanguage != true)?"Pengiriman Lokal":"Local Delivery", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 12, color: Colors.white, fontWeight: FontWeight.w500))),
                              ),
                            ),
                            Divider(thickness: 2,),
                            Text((isLanguage != true)?"Harga Retail":"Retail Price", style: GoogleFonts.poppins(
                                textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.grey))),
                            Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?250000:16.8), style: GoogleFonts.poppins(textStyle: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.grey, fontSize: 14),)),
                            Text((isLanguage != true)?"Harga Ecommerce":"E commerce Price", style: GoogleFonts.poppins(
                                textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                            Row(
                              children: [
                                Container(
                                  height: MediaQuery.of(context).size.height / 32,
                                  width: MediaQuery.of(context).size.width / 12,
                                  decoration: BoxDecoration(
                                      color: Color(0xff872934),
                                      borderRadius: BorderRadius.circular(4)
                                  ),
                                  child: Center(
                                    child: Text("10%", style: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 12, color: Colors.white, fontWeight: FontWeight.w500))),
                                  ),
                                ),
                                SizedBox(width: 4,),
                                Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?150000:10), style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 16),)),
                              ],
                            ),
                            SizedBox(height: 8,),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 14),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                      height: MediaQuery.of(context).size.height / 20,
                                      width: MediaQuery.of(context).size.width / 8,
                                      decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius: BorderRadius.circular(6.0),
                                          border: Border.all(color: Color(0xffe3cea3))
                                      ),
                                      child: Material(
                                        borderRadius: BorderRadius.circular(6.0),
                                        color: Colors.transparent,
                                        child: AnimatedSwitcher(
                                            duration: Duration.zero,
                                            child: InkWell(
                                              onTap: (){
                                                Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: new ChatSellerActivity(isLanguage, isCurrency)) );
                                              },
                                              splashColor: Colors.white,
                                              borderRadius: BorderRadius.circular(6.0),
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 0, vertical: 0),
                                                  child: Image.asset("assets/image/icon_chat.png", height: 24, width: 24,),
                                                ),
                                              ),
                                            )
                                        ),
                                      )
                                  ),
                                  Container(
                                      height: MediaQuery.of(context).size.height / 20,
                                      width: MediaQuery.of(context).size.width / 2.8,
                                      decoration: BoxDecoration(
                                          color: Color(0xffe3cea3),
                                          borderRadius: BorderRadius.circular(6.0),
                                          border: Border.all(color: Color(0xffe3cea3))
                                      ),
                                      child: Material(
                                        borderRadius: BorderRadius.circular(6.0),
                                        color: Colors.transparent,
                                        child: AnimatedSwitcher(
                                            duration: Duration.zero,
                                            child: InkWell(
                                              onTap: () async{

                                              },
                                              splashColor: Colors.white,
                                              borderRadius: BorderRadius.circular(6.0),
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 12.0, vertical: 4.0),
                                                  child: Text((isLanguage != true)?"Beli Langsung":"Buy Now", style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 14),)),
                                                ),
                                              ),
                                            )
                                        ),
                                      )
                                  ),
                                  Container(
                                      height: MediaQuery.of(context).size.height / 20,
                                      width: MediaQuery.of(context).size.width / 2.8,
                                      decoration: BoxDecoration(
                                          color: Color(0xff672934),
                                          borderRadius: BorderRadius.circular(6.0),
                                          border: Border.all(color: Color(0xffe3cea3))
                                      ),
                                      child: Material(
                                        borderRadius: BorderRadius.circular(6.0),
                                        color: Colors.transparent,
                                        child: AnimatedSwitcher(
                                            duration: Duration.zero,
                                            child: InkWell(
                                              onTap: () async{

                                              },
                                              splashColor: Colors.white,
                                              borderRadius: BorderRadius.circular(6.0),
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 12.0, vertical: 4.0),
                                                  child: Text((isLanguage != true)?"+Keranjang":"+Cart", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.white, fontSize: 14),)),
                                                ),
                                              ),
                                            )
                                        ),
                                      )
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 8,),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 14),
                              child: Container(
                                  height: MediaQuery.of(context).size.height / 24,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Colors.grey[300],
                                    borderRadius: BorderRadius.circular(6.0),
                                  ),
                                  child: Material(
                                    borderRadius: BorderRadius.circular(6.0),
                                    color: Colors.transparent,
                                    child: AnimatedSwitcher(
                                        duration: Duration.zero,
                                        child: InkWell(
                                          onTap: () async{

                                          },
                                          splashColor: Colors.white,
                                          borderRadius: BorderRadius.circular(6.0),
                                          child: Center(
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 12.0),
                                              child: Text((isLanguage != true)?"pengiriman: dihitung setelah memilih negara dan kuantitas":"shipping: calculated after selecting the country and quantity", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontSize: 10),)),
                                            ),
                                          ),
                                        )
                                    ),
                                  )
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                floating: true,
                pinned: true,
                snap: false,
                primary: true,
                expandedHeight: MediaQuery.of(context).size.height /1.5,
                bottom: TabBar(
                  controller: _tabController,
                  indicatorColor: Color(0xff872934),
                  labelColor: Color(0xff872934),
                  unselectedLabelColor: Colors.grey,
                  tabs: [
                    Tab(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 4,),
                        Container(
                          child: Text((isLanguage != true)?"Artis":"Artist", style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14))),
                        ),
                        Container(
                          child: Text((isLanguage != true)?"Produk":"Product", style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14))),
                        ),
                      ],
                    ),),
                    Tab(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 4,),
                        Container(
                          child: Text((isLanguage != true)?"Produk":"Product", style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14))),
                        ),
                        Container(
                          child: Text("Detail", style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14))),
                        ),
                      ],
                    ),),
                    Tab(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 4,),
                        Container(
                          child: Text((isLanguage != true)?"Ulasan":"User", style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14))),
                        ),
                        Container(
                          child: Text((isLanguage != true)?"Pengguna":"Review", style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14))),
                        ),
                      ],
                    ),),
                  ],
                ),
              ),
            ];
          },
          body: TabBarView(
              controller: _tabController,
              children: [
                new SafeArea(
                  top: false,
                  bottom: false,
                  child: Builder(
                    builder: (BuildContext context) {
                      return CustomScrollView(
                        slivers: <Widget>[
                          SliverPadding(
                            padding: const EdgeInsets.all(8.0),
                            sliver: SliverList(
                              delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                                return Column(
                                  children: <Widget>[
                                    Container(
                                      height: 90,
                                      width: double.infinity,
                                      color: Colors.blueGrey,
                                      child: Column(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text('$index'),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                  ],
                                );
                              },
                                childCount: 10,
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                new SafeArea(
                  top: false,
                  bottom: false,
                  child: Builder(
                    builder: (BuildContext context) {
                      return CustomScrollView(
                        slivers: <Widget>[
                          SliverPadding(
                            padding: const EdgeInsets.all(8.0),
                            sliver: SliverList(
                              delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                                return Column(
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: MediaQuery.of(context).size.height/ 2,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: (index == 0)?AssetImage("assets/image/parfume_trend.png"):
                                          AssetImage("assets/image/parfume_sakura.png"),
                                          fit: BoxFit.cover
                                        )
                                      ),
                                    ),
                                    SizedBox(height: 4,),
                                  ],
                                );
                              },
                                childCount: 5,
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                new SafeArea(
                  top: false,
                  bottom: false,
                  child: Builder(
                    builder: (BuildContext context) {
                      return CustomScrollView(
                        slivers: <Widget>[
                          SliverPadding(
                            padding: const EdgeInsets.all(8.0),
                            sliver: SliverList(
                              delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                                return CustomCard.reviewProductDetailCard(context, index, isLanguage);
                              },
                                childCount: 10,
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ]
          ),
        ),
      ),
    );
  }
}
