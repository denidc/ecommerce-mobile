import 'package:ecommerce/ui/feed/feed_explore_page.dart';
import 'package:ecommerce/utils/custom_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class ArtistDetailActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  ArtistDetailActivity(this.isLanguage, this.isCurrency);

  @override
  _ArtistDetailActivityState createState() => _ArtistDetailActivityState(isLanguage, isCurrency);
}

class _ArtistDetailActivityState extends State<ArtistDetailActivity> with SingleTickerProviderStateMixin {
  bool isLanguage;
  bool isCurrency;

  _ArtistDetailActivityState(this.isLanguage, this.isCurrency);

  TabController _tabController;
  ScrollController _scrollViewController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: 2,
      vsync: this,
    );
    _scrollViewController =ScrollController();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
    _scrollViewController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 8),
          child: Text((isLanguage != true)?"Detail Artis":"Artist Detail", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){},
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      body: DefaultTabController(
        length: _tabController.length,
        child: NestedScrollView(
          headerSliverBuilder:
              (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor: Colors.white,
                flexibleSpace: FlexibleSpaceBar(
                  background: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height/ 12,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Anna Raulee", style: GoogleFonts.poppins(
                                      textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold))),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text((isLanguage != true)?"Pengunjung  ":"Visitor ", style: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))),
                                      Text("25.7k", style: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12, color: Color(0xff872934), fontWeight: FontWeight.w500))),
                                      SizedBox(width: 8,),
                                      Text((isLanguage != true)?"Pelanggan  ":"Subscribers ", style: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))),
                                      Text("10k", style: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12, color: Color(0xff872934), fontWeight: FontWeight.w500))),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 14),
                                child: GestureDetector(
                                  onTap: (){
                                    print("Subscribe");
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width/4,
                                    height: MediaQuery.of(context).size.height,
                                    decoration: BoxDecoration(
                                        color: Color(0xff872934),
                                        borderRadius: BorderRadius.circular(10)
                                    ),
                                    child: Center(
                                      child: Text((isLanguage != true)?"Langganan":"Subscribe", style: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white))),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 4,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/image/seli_marchelia.png"),
                                fit: BoxFit.cover
                            )
                        ),
                      ),
                      SizedBox(height: 4,),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Bisa bayar pake ovo, transfer, kartu kredit, alfamart, doku, dll.", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))),
                              Text("instagram.com/anaraulee", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 12, color: Color(0xff872934), fontWeight: FontWeight.w500))),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                floating: true,
                pinned: true,
                snap: false,
                primary: true,
                expandedHeight: MediaQuery.of(context).size.height /2.1,
                leading: Container(),
                bottom: TabBar(
                  controller: _tabController,
                  indicatorColor: Color(0xff872934),
                  labelColor: Color(0xff872934),
                  unselectedLabelColor: Colors.grey,
                  tabs: [
                    Tab(child: Text((isLanguage != true)?"Umpan":"Feed", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),),
                    Tab(child: Text((isLanguage != true)?"Produk":"Product", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),),
                  ],
                ),
              ),
            ];
          },
          body: TabBarView(
            controller: _tabController,
            children: [
                new SafeArea(
                  top: false,
                  bottom: false,
                  child: Builder(
                    builder: (BuildContext context) {
                      return CustomScrollView(
                        slivers: <Widget>[
                        SliverPadding(
                          padding: const EdgeInsets.all(8.0),
                          sliver: SliverList(
                            delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                                return Column(
                                  children: <Widget>[
                                    Container(
                                      height: 90,
                                      width: double.infinity,
                                      color: Colors.blueGrey,
                                      child: Column(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text('$index'),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                  ],
                                );
                              },
                              childCount: 10,
                            ),
                          ),
                        ),
                        ],
                      );
                    },
                  ),
                ),
              new SafeArea(
                top: false,
                bottom: false,
                child: Builder(
                  builder: (BuildContext context) {
                    return CustomScrollView(
                      slivers: <Widget>[
                        SliverPadding(
                          padding: const EdgeInsets.all(8.0),
                          sliver: SliverList(
                            delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                              return CustomCard.productArtistDetail(context, isLanguage, isCurrency);
                            },
                              childCount: 10,
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ]
          ),
        ),
      ),
    );
  }
}