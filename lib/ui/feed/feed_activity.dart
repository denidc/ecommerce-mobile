import 'package:ecommerce/ui/feed/feed_explore_page.dart';
import 'package:ecommerce/ui/feed/feed_update_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FeedActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  FeedActivity(this.isLanguage, this.isCurrency);

  @override
  _FeedActivityState createState() => _FeedActivityState(isLanguage, isCurrency);
}

class _FeedActivityState extends State<FeedActivity> with SingleTickerProviderStateMixin{
  bool isLanguage;
  bool isCurrency;

  _FeedActivityState(this.isLanguage, this.isCurrency);

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: 2,
      vsync: this,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: _tabController.length,
        child: Scaffold(
          appBar: new PreferredSize(
            preferredSize: Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height/8) ,
            child: new TabBar(
              controller: _tabController,
              indicatorColor: Color(0xff872934),
              labelColor: Color(0xff872934),
              unselectedLabelColor: Colors.grey,
              tabs: [
                Tab(child: Text("Favorit artis", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),),
                Tab(child: Text("Update", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 18))),),
              ],
            ),
          ),
          body: TabBarView(
            controller: _tabController,
            children: [
              new FeedExplorePage(isLanguage),
              new FeedUpdatePage(isLanguage, isCurrency)
            ],
          ),
        ),
      ),
    );
  }
}
