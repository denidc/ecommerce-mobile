import 'package:ecommerce/utils/custom_card.dart';
import 'package:flutter/material.dart';

class FeedUpdatePage extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  FeedUpdatePage(this.isLanguage, this.isCurrency);

  @override
  _FeedUpdatePageState createState() => _FeedUpdatePageState(isLanguage, isCurrency);
}

class _FeedUpdatePageState extends State<FeedUpdatePage> {
  bool isCurrency;
  bool isLanguage;

  _FeedUpdatePageState(this.isLanguage, this.isCurrency);

  @override
  Widget build(BuildContext context) {
    List<Widget> _tiles = <Widget>[
      FeedUpdateCard(true, false, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","", isLanguage, isCurrency),
      FeedUpdateCard(true, true, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","https://www.youtube.com/watch?v=i1IKnWDecwA", isLanguage,  isCurrency),
      FeedUpdateCard(true, false, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","", isLanguage, isCurrency),
      FeedUpdateCard(true, true, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","https://www.youtube.com/watch?v=sVZpHFXcFJw", isLanguage, isCurrency),
      FeedUpdateCard(true, false, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","", isLanguage, isCurrency),
      FeedUpdateCard(true, true, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","https://www.youtube.com/watch?v=r6zIGXun57U", isLanguage, isCurrency),
    ];
    return Scaffold(
      body: SafeArea(
        bottom: true,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: ListView(
              children: _tiles,
            ),
          ),
        ),
      ),
    );
  }
}
