import 'package:ecommerce/utils/custom_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FeedExplorePage extends StatefulWidget {
  bool isLanguage;

  FeedExplorePage(this.isLanguage);

  @override
  _FeedExplorePageState createState() => _FeedExplorePageState(isLanguage);
}

class _FeedExplorePageState extends State<FeedExplorePage> {
  bool isLanguage;

  _FeedExplorePageState(this.isLanguage);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2.3;
    final double itemWidth = size.width / 2;
    List<Widget> _tiles = <Widget>[
      FeedExploreCard(isLanguage),
      FeedExploreCard(isLanguage),
      FeedExploreCard(isLanguage),
      FeedExploreCard(isLanguage),
      FeedExploreCard(isLanguage),
      FeedExploreCard(isLanguage),
    ];
    return Scaffold(
      body: SafeArea(
        bottom: true,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: GridView.count(
              crossAxisCount: 2,
              shrinkWrap: true,
              childAspectRatio: (itemWidth / itemHeight),
              children: _tiles,
            ),
          ),
        ),
      ),
    );
  }
}
