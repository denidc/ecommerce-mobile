import 'package:ecommerce/ui/main_activity.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  bool isLanguage;
  bool isCurrency;

  Future<bool> _getLanguage() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool("is_language") ?? false;
  }

  Future<bool> _getCurrency() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool("is_currency") ?? false;
  }

  Future<bool> _checkForSession() async{
    await Future.delayed(Duration(milliseconds: 1000), () {});

    return true;
  }

  @override
  void initState() {
    _getLanguage().then((value) {
      setState(() {
        isLanguage = value;
      });
    });
    _getCurrency().then((value) {
      setState(() {
        isCurrency = value;
      });
    });
    _checkForSession().then((status) {
      if(status){
        Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: MainActivity(isLanguage, isCurrency, true)) );
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/image/bg_auth.png"),
            fit: BoxFit.cover
          )
        ),
        child: Center(
          child: Text((isLanguage != true)?"Selamat Datang":"Welcome", style: TextStyle(fontSize: 32),),
        ),
      ),
    );
  }
}
