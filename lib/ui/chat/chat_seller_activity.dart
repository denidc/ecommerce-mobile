import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce/utils/full_image.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:page_transition/page_transition.dart';

class ChatSellerActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  ChatSellerActivity(this.isLanguage, this.isCurrency);

  @override
  _ChatSellerActivityState createState() => _ChatSellerActivityState(isLanguage, isCurrency);
}

class _ChatSellerActivityState extends State<ChatSellerActivity> {
  bool isLanguage;
  bool isCurrency;

  _ChatSellerActivityState(this.isLanguage, this.isCurrency);

  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  TextEditingController messageController = TextEditingController();
  FocusNode _focus;
  ScrollController scrollController;
  List<Widget> messages = [];

  File imageFile;
  final picker = ImagePicker();
  String imageUrl;
  bool isLoading = true;
  String _from = "user";

  @override
  void initState() {
    scrollController = new ScrollController();
    _focus = new FocusNode();
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels == 0){
          print("TOP");
        }else{
          print("BOTTOM");
        }
    }
    });
    super.initState();
  }

  @override
  void dispose() {
    scrollController.dispose();
    _focus.dispose();
    messageController.dispose();
    super.dispose();
  }

  Future notif(String text)async{

    var status = await OneSignal.shared.getPermissionSubscriptionState();

    var playerId = status.subscriptionStatus.userId;

    await OneSignal.shared.postNotification(OSCreateNotification(
      playerIds: [playerId],
      heading: "Ini Pesan",
      content: text.toString(),
      androidChannelId: "9a767e3b-b890-4438-aa1b-a593ee86834e",
    ));
  }

  Future uploadFile() async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    StorageReference reference = FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = reference.putFile(imageFile);
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
    setState(() {
      isLoading = true;
    });
    storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
      imageUrl = downloadUrl;
      setState(() async{
        messageController.clear();
        await _firestore.collection("room")
            .doc("1")
            .collection('messages').add({
          'type': "1",
          'text': "",
          'img': imageUrl,
          'from': _from,
          'date': DateTime.now().toIso8601String().toString(),
        });
        scrollController.animateTo(
          scrollController.position.maxScrollExtent,
          curve: Curves.fastOutSlowIn,
          duration: const Duration(milliseconds: 300),
        );
      });
    }, onError: (err) {
      Fluttertoast.showToast(
          msg: err.toString(),
          backgroundColor: Colors.grey,
          textColor: Colors.black,
          fontSize: 16.0
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    Future<void> callback() async {
      String txt;
      notif(messageController.text);
      if (messageController.text.length > 0) {
        txt = messageController.text;
        messageController.clear();
        await _firestore.collection("room")
            .doc("1")
            .collection('messages').add({
          'type': "0",
          'text': txt,
          'img': "",
          'from': _from,
          'date': DateTime.now().toIso8601String().toString(),
        });
        scrollController.animateTo(
          150 / double.parse(messages.length.toString()),
          curve: Curves.fastOutSlowIn,
          duration: const Duration(milliseconds: 300),
        );
      }
    }

    Future getImage() async {
      final pickedFile = await picker.getImage(source: ImageSource.gallery);
      print(pickedFile);
      setState(() {
        imageFile = File(pickedFile.path);
        isLoading = false;
        Fluttertoast.showToast(
            msg: "Tunggu beberapa saat...",
            backgroundColor: Colors.grey,
            textColor: Colors.black,
            fontSize: 16.0
        );
      });

      uploadFile();
    }
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        flexibleSpace: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(top: 4),
            child: Row(
              children: [
                IconButton(
                  onPressed: (){},
                  icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
                ),
                CircleAvatar(           // <-- avatar here instead
                  radius: 20,
                  backgroundImage: AssetImage("assets/image/seli_marchelia.png"),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Seli Marchelia", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Color(0xff872934), fontWeight: FontWeight.bold)),),
                    Text("Online", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 12, color: Colors.grey)),)
                  ],
                ),
              ],
            ),
          ),
        ),
        actions: [
          PopupMenuButton<int>(
            icon: Icon(Icons.more_vert, color: Colors.grey[600],),
            offset: Offset(50,40),
            itemBuilder: (context) => [
              PopupMenuItem(
                value: 1,
                child: Text(
                  "Opsi",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.w700),
                ),
              ),
              PopupMenuItem(
                value: 2,
                child: Text(
                  "Opsi 2",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.w700),
                ),
              ),
            ],
          )
        ],
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            RaisedButton(
              onPressed: (){
                if(_from == "user"){
                  setState(() {
                    _from = "seller";
                  });
                }else if(_from == "seller"){
                  setState(() {
                    _from = "user";
                  });
                }
              },
              child: Text("Menjadi " + _from),
            ),
            (isLoading != false)?Expanded(
              child: StreamBuilder<QuerySnapshot>(
                stream: _firestore.collection("room")
                    .doc("1")
                    .collection('messages')
                    .orderBy('date')
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(
                      child: CircularProgressIndicator(),
                    );

                  List<DocumentSnapshot> docs = snapshot.data.docs;

                  messages = docs
                      .map((doc) => Message(
                    type: doc.get('type'),
                    from: doc.get('from'),
                    text: doc.get('text'),
                    img: doc.get('img'),
                    me: "user" == doc.get('from'),
                    date: doc.get('date'),
                  ))
                      .toList();
                  return SingleChildScrollView(
                    controller: scrollController,
                    reverse: true,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: ListView(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        controller: scrollController,
                        children: <Widget>[
                          ...messages,
                          SizedBox(height: 10,)
                        ],
                      ),
                    ),
                  );
                },
              ),
            ) : Expanded(
              child: Container(child: Center(child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.grey),
              ))),
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.grey[400]
              ),
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Row(
                      children: [
                        Container(
                          width: 70,
                          height: 70,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("assets/image/parfume_sakura.png"),
                              fit: BoxFit.cover
                            )
                          ),
                        ),
                        SizedBox(width: 4,),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("[LIPS] Liberty Fluid Matte Lip Paint", style: GoogleFonts.poppins(
                                textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold,))),
                            Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?150000:10), style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 16),)),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            (true)?Padding(
              padding: const EdgeInsets.only(left: 4, bottom: 2, top: 8),
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: ()async{
                              messageController.clear();
                              await _firestore.collection("room")
                                  .doc("1")
                                  .collection('messages').add({
                                'type': "0",
                                'text': "Hai, barang ready?",
                                'img': "",
                                'from': _from,
                                'date': DateTime.now().toIso8601String().toString(),
                              });
                              scrollController.animateTo(
                                150 / double.parse(messages.length.toString()),
                                curve: Curves.fastOutSlowIn,
                                duration: const Duration(milliseconds: 300),
                              );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(color: Color(0xff872934)),
                                borderRadius: BorderRadius.circular(8)
                              ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Hai, barang ready?", style: GoogleFonts.poppins(
                                      textStyle: TextStyle(fontSize: 12, color: Color(0xff872934))),),
                                )
                            ),
                          ),
                          SizedBox(width: 8,),
                          GestureDetector(
                            onTap: ()async{
                              messageController.clear();
                              await _firestore.collection("room")
                                  .doc("1")
                                  .collection('messages').add({
                                'type': "0",
                                'text': "Bisa dikirim hari ini ga?",
                                'img': "",
                                'from': _from,
                                'date': DateTime.now().toIso8601String().toString(),
                              });
                              scrollController.animateTo(
                                150 / double.parse(messages.length.toString()),
                                curve: Curves.fastOutSlowIn,
                                duration: const Duration(milliseconds: 300),
                              );
                            },
                            child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(color: Color(0xff872934)),
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Bisa dikirim hari ini ga?", style: GoogleFonts.poppins(
                                      textStyle: TextStyle(fontSize: 12, color: Color(0xff872934))),),
                                )
                            ),
                          ),
                          SizedBox(width: 8,),
                          GestureDetector(
                            onTap: ()async{
                              messageController.clear();
                              await _firestore.collection("room")
                                  .doc("1")
                                  .collection('messages').add({
                                'type': "0",
                                'text': "Terima Kasih",
                                'img': "",
                                'from': _from,
                                'date': DateTime.now().toIso8601String().toString(),
                              });
                              scrollController.animateTo(
                                150 / double.parse(messages.length.toString()),
                                curve: Curves.fastOutSlowIn,
                                duration: const Duration(milliseconds: 300),
                              );
                            },
                            child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(color: Color(0xff872934)),
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Terima Kasih", style: GoogleFonts.poppins(
                                      textStyle: TextStyle(fontSize: 12, color: Color(0xff872934))),),
                                )
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 8,),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              focusNode: _focus,
                              onSubmitted: (value) => callback(),
                              cursorColor: Colors.grey,
                              style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 14)),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(left: 12),
                                hintText: "Tulis pesan...",
                                hintStyle: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide(color: Colors.grey, width: 2),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide(color: Colors.grey, width: 2),
                                ),
                                suffixIcon: IconButton(
                                  icon: Icon(Icons.image, color: Colors.grey,),
                                  onPressed: getImage,
                                ),
                              ),
                              controller: messageController,
                            ),
                          ),
                          SendButton(
                            text: "Send",
                            callback: callback,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ) : Container(
                decoration: BoxDecoration(
                    color: Color(0xffff9234)
                ),
                child: Center(child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0),
                  child: Text("Wait confirmation from the restaurant", style: TextStyle(color: Colors.white),),
                ))
            ),
          ],
        ),
      ),
    );
  }
}

class Message extends StatelessWidget {
  final String type;
  final String from;
  final String text;
  final String img;
  final String date;

  final bool me;

  const Message({Key key, this.type, this.from, this.text, this.img, this.me, this.date}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: (type != "1")?Column(
        crossAxisAlignment:
        me ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Material(
            color: me ? Color(0xffe3cea3) : Colors.grey[300],
            borderRadius: me ? BorderRadius.only(topLeft: Radius.circular(20),
                bottomLeft: Radius.circular(20), topRight: Radius.circular(20),
                bottomRight: Radius.circular(0)) : BorderRadius.only(topLeft: Radius.circular(0),
                bottomLeft: Radius.circular(20), topRight: Radius.circular(20), bottomRight: Radius.circular(20)),
            elevation: 1.0,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: me ? Text(
                text, style: GoogleFonts.poppins(
                  textStyle: TextStyle(fontSize: 12)),
              ) : Text(
                text, style: GoogleFonts.poppins(
                  textStyle: TextStyle(fontSize: 12)),
              ),
            ),
          ),
          Padding(
            padding: me ? EdgeInsets.only(right: 4) : EdgeInsets.only(left: 4),
            child: Text(date.substring(11,16), style: GoogleFonts.poppins(
                textStyle: TextStyle(fontSize: 12)),),
          ),
          SizedBox(height: 10,)
        ],
      ) : Column(
        crossAxisAlignment:
        me ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            onTap: (){
              Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: FullImage(img)));
            },
            child: Container(
              height: 150,
              width: 150,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(5),
                  image: DecorationImage(
                    image: NetworkImage(img),
                    fit: BoxFit.cover,
                  )
              ),
            ),
          ),
          Text(date.substring(11,16), style: GoogleFonts.poppins(
              textStyle: TextStyle(fontSize: 12)),),
          SizedBox(height: 10,)
        ],
      ),
    );
  }
}

class SendButton extends StatelessWidget {
  final String text;
  final VoidCallback callback;

  const SendButton({Key key, this.text, this.callback}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xff872934),
          borderRadius: BorderRadius.circular(100)
        ),
        child: IconButton(
          color: Colors.white,
          onPressed: callback,
          icon: Icon(Icons.send),
          iconSize: 30,
          splashColor: Colors.white,
        ),
      ),
    );
  }
}
