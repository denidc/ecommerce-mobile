import 'dart:convert';

import 'package:ecommerce/model/search.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;

class SearchActivity extends StatefulWidget {
  bool isLanguage;

  SearchActivity(this.isLanguage);

  @override
  _SearchActivityState createState() => _SearchActivityState(isLanguage);
}

class _SearchActivityState extends State<SearchActivity> {
  bool isLanguage;

  _SearchActivityState(this.isLanguage);

  List<Search> _list = [];
  List<Search> _search = [];
  List<String> _history = [];
  var loading = false;
  bool isEmpty = true;

  Future<Null> fetchData() async {
    setState(() {
      loading = true;
    });
    _list.clear();
    final response =
    await http.get("https://jsonplaceholder.typicode.com/posts");
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        for (Map i in data) {
          _list.add(Search.formJson(i));
          loading = false;
        }
      });
    }
  }

  TextEditingController controller = new TextEditingController();

  onSearch(String text) async {
    _search.clear();
    if (text.isEmpty) {
      setState(() {
        isEmpty = true;
      });
      return;
    }

    setState(() {
      isEmpty = false;
    });

    _list.forEach((f) {
      if (f.title.contains(text))
        _search.add(f);
    });
  }

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10.0),
                child: TextField(
                  controller: controller,
                  onChanged: onSearch,
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search, color: Color(0xff872934),),
                    suffixIcon: IconButton(
                      icon: Icon(Icons.close, color: Color(0xff872934),),
                      onPressed: (){
                        controller.clear();
                        onSearch('');
                      },
                    ),
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: (isLanguage != true)?"Apa yang kamu cari...":"What are you looking for...",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Color(0xff872934)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Color(0xff872934)),
                    ),
                  ),
                ),
              ),
              loading ? Expanded(
                    child: Center(
                child: CircularProgressIndicator(),
                ),
                  ) : isEmpty ? Expanded(
                    child: ListView.builder(
                      itemCount: _history.length,
                        itemBuilder: (context, index){
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 12),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(_history[index], style: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 14)),),
                                Divider(),
                              ],
                            ),
                          );
                        }
                    )
                ) : (_search.length != 0) ? Expanded(
                child: controller.text.isNotEmpty
                    ? ListView.builder(
                  itemCount: _search.length,
                  itemBuilder: (context, i) {
                    final b = _search[i];
                    return GestureDetector(
                      onTap: (){
                        _history.add(b.title);
                      },
                      child: Container(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                b.title,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0),
                              ),
                              SizedBox(
                                height: 4.0,
                              ),
                              Text(b.body),
                            ],
                          )),
                    );
                  },
                ) : Container(),
              ) : Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 80,
                        height: 80,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xff872934)
                        ),
                        child: Icon(Icons.close, color: Colors.white, size: 60,),
                      ),
                      SizedBox(height: 4,),
                      Text((isLanguage != true)?"Produk tidak ditemukan":"Product not found", style: GoogleFonts.poppins(
                          textStyle: TextStyle(color: Colors.grey[700], fontSize: 20, fontWeight: FontWeight.bold)),),
                      SizedBox(height: 18,),
                      Container(
                          decoration: BoxDecoration(
                            color: Color(0xff872934),
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          child: Material(
                            borderRadius: BorderRadius.circular(4.0),
                            color: Colors.transparent,
                            child: AnimatedSwitcher(
                                duration: Duration.zero,
                                child: InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  splashColor: Colors.white,
                                  borderRadius: BorderRadius.circular(4.0),
                                  child: Center(
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 22.0, vertical: 4.0),
                                      child: Text((isLanguage != true)?"Kembali ke Rumah":"Back Home", style: GoogleFonts.poppins(
                                          textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                                    ),
                                  ),
                                )
                            ),
                          )
                      ),
                    ],
                  ),
                )
              ),
            ],
          ),
        ),
      ),
    );
  }
}
