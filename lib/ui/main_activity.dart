import 'dart:async';
import 'dart:io';

import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:connectivity/connectivity.dart';
import 'package:ecommerce/ui/feed/feed_activity.dart';
import 'package:ecommerce/ui/home/home_activity.dart';
import 'package:ecommerce/ui/login%20and%20regis/login_activity.dart';
import 'file:///C:/Users/user/AndroidStudioProjects/ecommerce/lib/ui/setting/user/profile_page.dart';
import 'package:ecommerce/ui/setting/setting_activity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class MainActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;
  bool isPopUpAds;

  MainActivity(this.isLanguage, this.isCurrency, this.isPopUpAds);

  @override
  _MainActivityState createState() => _MainActivityState(isLanguage, isCurrency, isPopUpAds);
}

class _MainActivityState extends State<MainActivity> {
  bool isLanguage;
  bool isCurrency;
  bool isPopUpAds;

  _MainActivityState(this.isLanguage, this.isCurrency, this.isPopUpAds);

  DateTime currentBackPressTime;
  var _bottomNavIndex = 0;
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(
          msg: (isLanguage != true)?"Tekan kembali lagi untuk keluar dari aplikasi":"Press again to exit the application",
          backgroundColor: Colors.grey[300],
          textColor: Colors.black,
          fontSize: 14.0
      );
      return Future.value(false);
    }
    if(Platform.isAndroid){
      SystemNavigator.pop();
    }else if(Platform.isIOS){
      exit(0);
    }
    return Future.value(true);
  }

  Future<void> _checkConnectivity() async{
    await (Connectivity().checkConnectivity());
  }

  @override
  void initState() {
    _checkConnectivity();

    _connectivitySubscription = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.mobile) {
        Fluttertoast.showToast(
            msg: (isLanguage != true)?"Anda berada di koneksi mobile network":"You are on a mobile network connection",
            backgroundColor: Colors.grey[300],
            textColor: Colors.black,
            fontSize: 14.0
        );
      } else if (result == ConnectivityResult.wifi) {
        Fluttertoast.showToast(
            msg: (isLanguage != true)?"Anda berada di koneksi wifi network":"You are on a wifi network connection",
            backgroundColor: Colors.grey[300],
            textColor: Colors.black,
            fontSize: 14.0
        );
      }else{
        Fluttertoast.showToast(
            msg: (isLanguage != true)?"Cek koneksi internet anda":"Check your internet connection",
            backgroundColor: Colors.grey[300],
            textColor: Colors.black,
            fontSize: 14.0
        );
      }
    });
    OneSignal.shared.init(
        "b2aa025d-9354-42c8-ba6e-4a981b0c595e",
        iOSSettings: null
    );
    OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      // will be called whenever a notification is opened/button pressed.
    Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: MainActivity(isLanguage, isCurrency, true)));
    });
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  void _showDialog(BuildContext context){
    showDialog(
        context: context,
        builder: (_) => GestureDetector(
          onTap: (){
            setState(() {
              isPopUpAds = false;
            });
            Navigator.of(context, rootNavigator: true).pop();
          },
          child: Material(
            type: MaterialType.transparency,
            child: Center(
              child: GestureDetector(
                onTap: (){
//                  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ProfilePage()));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width / 1.5,
                  height: MediaQuery.of(context).size.height / 2,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/image/parfume_sakura.png"),
                      fit: BoxFit.fill
                    ),
                    borderRadius: BorderRadius.circular(8)
                  ),
                  child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              SizedBox(height: 8,),
                              Text((isLanguage != true)?"Parfum Terbaru":"New Perfume", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold)),),
                            ],
                          ),
                          Container(
                              decoration: BoxDecoration(
                                color: Color(0xffe3cea3),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(6.0),
                                color: Colors.transparent,
                                child: AnimatedSwitcher(
                                    duration: Duration.zero,
                                    child: InkWell(
                                      onTap: () async{

                                      },
                                      splashColor: Colors.white,
                                      borderRadius: BorderRadius.circular(6.0),
                                      child: Center(
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 22.0, vertical: 4.0),
                                          child: Text((isLanguage != true)?"Beli Sekarang":"Buy Now", style: GoogleFonts.poppins(
                                              textStyle: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold)),),
                                        ),
                                      ),
                                    )
                                ),
                              )
                          ),
                        ],
                      )
                  ),
                ),
              ),
            ),
          ),
        ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if(isPopUpAds){
      Future.delayed(Duration.zero, () => _showDialog(context));
    }else{}
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        body: IndexedStack(
            index: _bottomNavIndex,
            children: [
              HomeActivity(isLanguage, isCurrency),
              FeedActivity(isLanguage, isCurrency),
              SettingActivity(isLanguage, isCurrency),
              HomeActivity(isLanguage, isCurrency),
            ]
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: (){
            Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: LoginActivity(isLanguage, isCurrency)) );
          },
          child: Icon(Icons.shopping_cart, color: Color(0xff872934),),
          backgroundColor: Color(0xffe3cea3),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _bottomNavIndex,
          onTap: (i){
            setState(() {
              _bottomNavIndex = i;
            });
          },
          type: BottomNavigationBarType.fixed,
          showUnselectedLabels: false,
          backgroundColor: Color(0xffe3cea3),
          unselectedItemColor: Colors.white,
          selectedItemColor: Color(0xff872934),
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home,),
              title: Text((isLanguage != true)?"Rumah":"Home", style: GoogleFonts.poppins(
                  textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w500)))
            ),
            BottomNavigationBarItem(
                icon: Padding(
                  padding: const EdgeInsets.only(right: 36),
                  child: Icon(Icons.portrait),
                ),
                title: Padding(
                  padding: const EdgeInsets.only(right: 36),
                  child: Text((isLanguage != true)?"Umpan":"Feed", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))),
                )
            ),
            BottomNavigationBarItem(
                icon: Padding(
                  padding: const EdgeInsets.only(left: 36),
                  child: Icon(Icons.settings,),
                ),
                title: Padding(
                  padding: const EdgeInsets.only(left: 36),
                  child: Text((isLanguage != true)?"Pengaturan":"Setting", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: (isLanguage != true)?9:12, fontWeight: FontWeight.w500))),
                )
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.bookmark_border,),
                title: Text((isLanguage != true)?"Langganan":"Subscribe", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w500)))
            ),
          ],
        )
      ),
    );
  }
}
