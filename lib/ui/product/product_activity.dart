import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:dropdown_search/dropdown_search.dart';

class ProductActivity extends StatefulWidget {
  @override
  _ProductActivityState createState() => _ProductActivityState();
}

class _ProductActivityState extends State<ProductActivity> {
  ScrollController _scrollViewController;
  String dropdownCategory = "All";
  String dropdownFacility = "Special";

  @override
  void initState() {
    super.initState();
    _scrollViewController =ScrollController();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollViewController.dispose();
  }

  List<String> _listPromo = [
    "assets/image/item_promo.png",
    "assets/image/item_promo.png",
    "assets/image/item_promo.png",
    "assets/image/item_promo.png",
  ];

  List<String> _list = [
    "assets/image/seli_marchelia.png",
    "assets/image/seli_marchelia.png",
    "assets/image/seli_marchelia.png",
    "assets/image/seli_marchelia.png",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 5),
          child: Text("Produk", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){},
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      body: NestedScrollView(
        headerSliverBuilder:
            (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              backgroundColor: Colors.white,
              flexibleSpace: FlexibleSpaceBar(
                background: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 4,),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text("Special Event", style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 16, fontWeight: FontWeight.bold),)),
                    ),
                    SizedBox(height: 8,),
                    Container(
                      height: MediaQuery.of(context).size.height/6,
                      width: MediaQuery.of(context).size.width,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: _listPromo.length,
                        itemBuilder: (context, index){
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Container(
                              height: MediaQuery.of(context).size.height/8,
                              width: MediaQuery.of(context).size.width/1.6,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(_listPromo[index]),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              floating: true,
              pinned: true,
              snap: false,
              expandedHeight: MediaQuery.of(context).size.height /3.5,
              leading: Container(),
              bottom: PreferredSize(
                preferredSize: Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height/14),
                child: IntrinsicHeight(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width/2.1,
                        child: DropdownSearch<String>(
                            mode: Mode.MENU,
                            popupBackgroundColor: Color(0xffe3cea3),
                            dropdownSearchDecoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(left: 8)
                            ),
                            showSelectedItem: true,
                            selectedItem: dropdownCategory,
                            items: ["All", "Today's Deal", "Skin Care", "Makeup", "Acne Care", "Body & Hair", "Etc", "Brand"],
                            onChanged: (v){
                              setState(() {
                                dropdownCategory = v;
                              });
                            },),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: VerticalDivider(
                          thickness: 2,
                          color: Colors.black,),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width/2.1,
                        child: DropdownSearch<String>(
                            mode: Mode.MENU,
                            popupBackgroundColor: Color(0xffe3cea3),
//                            showSearchBox: true,
                            dropdownSearchDecoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(left: 8),
                            ),
                            showSelectedItem: true,
                            selectedItem: dropdownFacility,
                            items: ["Special", "New Arrival", "Most Review", "Free Shipping"],
                            onChanged: (v){
                              setState(() {
                                dropdownFacility = v;
                              });
                            },),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ];
        },
        body: new SafeArea(
          top: false,
          bottom: false,
          child: Builder(
            builder: (BuildContext context) {
              return CustomScrollView(
                slivers: <Widget>[
                  SliverPadding(
                    padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 4),
                    sliver: SliverList(
                      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: MediaQuery.of(context).size.height / 4,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage("assets/image/parfume_trend.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Liberty LIP", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontSize: 14),)),
                                  Text("[LIPS] Liberty Fluid Matte Lip Paint", style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)),
                                  Row(
                                    children: [
                                      Container(
                                        height: MediaQuery.of(context).size.height / 24,
                                        width: MediaQuery.of(context).size.width/8,
                                        decoration: BoxDecoration(
                                            color: Color(0xff672934)
                                        ),
                                        child: Center(
                                          child: Text("-10%", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.white, fontSize: 14),)),
                                        ),
                                      ),
                                      SizedBox(width: 8,),
                                      Container(
                                        height: MediaQuery.of(context).size.height / 24,
                                        width: MediaQuery.of(context).size.width/2.8,
                                        decoration: BoxDecoration(
                                            color: Color(0xff672934)
                                        ),
                                        child: Center(
                                          child: Text("Pengiriman Lokal", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.white, fontSize: 14),)),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 4,),
                                  Row(
                                    children: [
                                      Text("Rp.300.000", style: GoogleFonts.poppins(textStyle: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.grey, fontSize: 14),)),
                                      SizedBox(width: 8,),
                                      Text("Rp.150.000", style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 16, fontWeight: FontWeight.bold),)),
                                    ],
                                  ),
                                  SizedBox(height: 12,),
                                  Row(
                                    children: [
                                      Text("Artis Review", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontSize: 12),)),
                                      SizedBox(width: 6,),
                                      Text("4", style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 12),)),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 4),
                            Container(
                              height: MediaQuery.of(context).size.height/8,
                              width: MediaQuery.of(context).size.width,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: _list.length,
                                itemBuilder: (context, index){
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 4),
                                    child: Container(
                                      height: MediaQuery.of(context).size.height/10,
                                      width: MediaQuery.of(context).size.width/3,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(_list[index]),
                                              fit: BoxFit.cover
                                          )
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                            SizedBox(height: 4),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 12),
                              child: Divider(thickness: 1, color: Colors.black,),
                            ),
                            SizedBox(height: 4),
                          ],
                        );
                      },
                        childCount: 10,
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
