import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce/ui/delivery_information/customer_information_activity.dart';
import 'package:ecommerce/ui/detail/product_detail_activity.dart';
import 'package:ecommerce/ui/product/product_activity.dart';
import 'package:ecommerce/ui/search/search_activity.dart';
import 'package:ecommerce/utils/custom_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:responsive_grid/responsive_grid.dart';

class HomeActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  HomeActivity(this.isLanguage, this.isCurrency);

  @override
  _HomeActivityState createState() => _HomeActivityState(isLanguage, isCurrency);
}

class _HomeActivityState extends State<HomeActivity> {
  bool isLanguage;
  bool isCurrency;

  _HomeActivityState(this.isLanguage, this.isCurrency);

  String descNewProduct = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.";
  bool canReadMore = true;
  double ratingCount = 5;
  ScrollController _scrollController;
  CarouselController _carouselController;

  int _currentHeader = 0;
  int _currentProduct = 0;

  List carouselHeader = [
    "assets/image/parfume_sakura.png",
    "assets/image/parfume_sakura.png",
    "assets/image/parfume_sakura.png",
    "assets/image/parfume_sakura.png",
  ];

  List carouselProduct = [
    "assets/image/seli_marchelia.png",
    "assets/image/seli_marchelia.png",
    "assets/image/seli_marchelia.png",
    "assets/image/seli_marchelia.png",
  ];

  List<String> informationHeader = [
    "Serum pembersih jerawat dijamin!!! Cuma 100rb aja :)",
    "Serum pembersih noda hitam dijamin!!! Cuma 200rb aja :)",
    "Serum pembersih bintik2 dijamin!!! Cuma 300rb aja :)",
    "Serum pembersih cacar dijamin!!! Cuma 400rb aja :)",
  ];

  List<String> subInformationHeader = [
    "Tersedia 2 varian saja yang dapat kamu sesuaikan dengan kebutuhanmu",
    "Tersedia 4 varian saja yang dapat kamu sesuaikan dengan kebutuhanmu",
    "Tersedia 8 varian saja yang dapat kamu sesuaikan dengan kebutuhanmu",
    "Tersedia 12 varian saja yang dapat kamu sesuaikan dengan kebutuhanmu",
  ];

  List<T> mapHeader<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }
  List<T> mapArtis<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

//  List<StaggeredTile> _staggeredTiles = const <StaggeredTile>[
//    const StaggeredTile.count(2, 2.5),
//    const StaggeredTile.count(2, 3),
//    const StaggeredTile.count(2, 3),
//    const StaggeredTile.count(2, 2.5),
//    const StaggeredTile.count(2, 2.5),
//    const StaggeredTile.count(2, 3),
//  ];

  @override
  void initState() {
    _scrollController = new ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

//    List<Widget> _tiles = <Widget>[
//      CustomCard.listHomeCard(context, false, "assets/image/seli_marchelia.png"),
//      CustomCard.listHomeCard(context, true, "assets/image/parfume_trend.png"),
//      CustomCard.listHomeCard(context, true, "assets/image/parfume_trend.png"),
//      CustomCard.listHomeCard(context, false, "assets/image/seli_marchelia.png"),
//      CustomCard.listHomeCard(context, false, "assets/image/seli_marchelia.png"),
//      CustomCard.listHomeCard(context, true, "assets/image/parfume_trend.png"),
//    ];

    List<Widget> _tilesHome = <Widget>[
      FeedUpdateCard(true, false, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","", isLanguage, isCurrency),
      FeedUpdateCard(true, true, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","https://www.youtube.com/watch?v=i1IKnWDecwA", isLanguage, isCurrency),
      FeedUpdateCard(true, false, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","", isLanguage, isCurrency),
      FeedUpdateCard(true, true, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","https://www.youtube.com/watch?v=sVZpHFXcFJw", isLanguage, isCurrency),
      FeedUpdateCard(true, false, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","", isLanguage, isCurrency),
      FeedUpdateCard(true, true, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam.","https://www.youtube.com/watch?v=r6zIGXun57U", isLanguage, isCurrency),
    ];

    //frame home
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //carousel header
              Container(
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: <Widget>[
                    CarouselSlider(
                      options: CarouselOptions(
                          autoPlay: true,
                          height: MediaQuery.of(context).size.height / 3,
                          scrollDirection: Axis.horizontal,
                          enableInfiniteScroll: true,
                          onPageChanged: (index, reason){
                            setState(() {
                              _currentHeader = index;
                            });
                          }
                      ),
                      items: carouselHeader.map((e) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(e),
                                fit: BoxFit.fill,
                              )
                          ),
                        );
                      }).toList(),
                    ),
                    Positioned(
                      left: MediaQuery.of(context).size.width / 2.2,
                      bottom: 4,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: mapHeader<Widget>(carouselHeader, (index, url) {
                          return Container(
                            width: _currentHeader == index ? 12 : 10.0,
                            height: _currentHeader == index ? 12 : 10.0,
                            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: _currentHeader == index ? Color(0xff872934) : Colors.white,
                            ),
                          );
                        }),
                      ),
                    ),
                    //text field search
                    Positioned(
                      top: -90,
                      bottom: 100,
                      right: 10,
                      child: Row(
                        children: <Widget>[
//                            Padding(
//                              padding: const EdgeInsets.only(),
//                              child: Container(
//                                width: 150 ,
//                                height: 40,
//                                decoration: BoxDecoration(
//                                  color: Colors.grey[200],
//                                  borderRadius: BorderRadius.circular(10),
//                                  boxShadow: [
//                                    BoxShadow(
//                                      color: Colors.grey.withOpacity(0.5),
//                                      spreadRadius: 1,
//                                      blurRadius: 3,
//                                      offset: Offset(0, 0), // changes position of shadow
//                                    ),
//                                  ],
//                                ),
//                                child: Padding(
//                                  padding: const EdgeInsets.symmetric(horizontal: 8),
//                                  child: TextField(
//                                    textInputAction: TextInputAction.search,
//                                    onSubmitted: (v){
//                                      Fluttertoast.showToast(
//                                          msg: v,
//                                          backgroundColor: Colors.grey[300],
//                                          textColor: Colors.black,
//                                          fontSize: 16.0
//                                      );
//                                    },
//                                    autocorrect: false,
//                                    controller: txSearch,
//                                    style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey[500], fontSize: 12)),
//                                    decoration: InputDecoration(
//                                        prefixIconConstraints:BoxConstraints(minWidth: 23, maxHeight: 20),
//                                        border: InputBorder.none,
//                                        prefixIcon: Padding(
//                                          padding: const EdgeInsets.only(right: 8.0, top: 2),
//                                          child: Icon(Icons.search, color: Colors.grey[600], size: 20,),
//                                        ),
//                                        hintText: 'Search',
//                                        hintStyle: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey[500], fontSize: 12)),
//                                    ),
//                                  ),
//                                ),
//                              ),
//                            ),
                          IconButton(
                            onPressed: (){
                              Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: new SearchActivity(isLanguage)) );
                            },
                            icon: Icon(Icons.search),
                          ),
                          SizedBox(width: 4,),
                          //menu category
                          Icon(Icons.menu)
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Stack(
                  children: mapHeader<Widget>(carouselHeader, (index, url) {
                    return Container(
                      width: _currentHeader == index ? MediaQuery.of(context).size.width : 0.0,
                      height: _currentHeader == index ? MediaQuery.of(context).size.height/12 : 0.0,
                      child: _currentHeader == index ?Text(
                          informationHeader[index], style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))
                      ):SizedBox(),
                    );
                  }),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Stack(
                  children: mapHeader<Widget>(carouselHeader, (index, url) {
                    return Container(
                      width: _currentHeader == index ? MediaQuery.of(context).size.width : 0.0,
                      height: _currentHeader == index ? MediaQuery.of(context).size.height/14 : 0.0,
                      child: _currentHeader == index ?Text(
                          subInformationHeader[index], style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14))
                      ):SizedBox(),
                    );
                  }),
                ),
              ),

              //part trend category
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height/3,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 20,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 240,
                        decoration: BoxDecoration(
                          color: Color(0xfffffaf0),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 4,
                      left: 20,
                      child: Container(
                        width: MediaQuery.of(context).size.width / 1.6,
                        height: 38,
                        decoration: BoxDecoration(
                            color: Color(0xff872934),
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(10), bottomRight: Radius.circular(10))
                        ),
                      ),
                    ),
                    Positioned(
                        top: 8,
                        left: (isLanguage != true)?48:38,
                        child: Text((isLanguage != true)?"Trendi Kategori":"Trendy Categories", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),)
                    ),
                    //list trend category
                    Padding(
                      padding: const EdgeInsets.only(top: 30),
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 18),
                          // child: ListView.builder(
                          //     itemCount: 5,
                          //     physics: BouncingScrollPhysics(),
                          //     scrollDirection: Axis.horizontal,
                          //     itemBuilder: (BuildContext context, index){
                          //       return CustomCard.trendCard(context, "Perfume", isCurrency);
                          //     }
                          // ),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: [
                                1,2,3,4,5
                              ].map((e) {
                                return GestureDetector(
                                  onTap: () {
                                    // Share.share("Look at this! Ayam Goreng Nelongso, Mulyosari on GoFood." + "\t" + "https://gofood.link/u/41AgM");
                                  },
                                  child: Container(
                                    height: MediaQuery.of(context).size.height / 4,
                                    width: MediaQuery.of(context).size.width / 3.2,
                                    margin: EdgeInsets.only(left: 8),
                                    decoration: BoxDecoration(
                                        color: Colors.white
                                    ),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              height: MediaQuery.of(context).size.height / 8,
                                              width: MediaQuery.of(context).size.width / 3.2,
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: AssetImage('assets/image/parfume_trend.png'),
                                                  )
                                              ),
                                            ),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                RatingBar(
                                                  itemSize: 18,
                                                  initialRating: ratingCount,
                                                  ignoreGestures: true,
                                                  minRating: 1,
                                                  direction: Axis.horizontal,
                                                  allowHalfRating: true,
                                                  itemCount: 5,
                                                  itemBuilder: (context, _) => Icon(
                                                    Icons.star,
                                                    color: Colors.amber,
                                                  ),
                                                  onRatingUpdate: (rating) {
                                                    print(rating);
                                                  },
                                                ),
                                                SizedBox(width: 4,),
                                                Text(ratingCount.toString(), style: GoogleFonts.poppins(
                                                    textStyle: TextStyle(fontSize: 10, color: Color(0xff872934))))
                                              ],
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 4),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: <Widget>[
                                              Text("Scrett Parfume", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),)),
                                              Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?300000:20), style: GoogleFonts.poppins(textStyle: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.grey, fontSize: 12),)),
                                              Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?150000:10), style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 12),)),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 12,),
              //part product artist
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width ,
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                width: MediaQuery.of(context).size.width / 1.4,
                                height: 38,
                                decoration: BoxDecoration(
                                    color: Color(0xff872934),
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10), bottomRight: Radius.circular(10))
                                ),
                              ),
                            ),
                            Positioned(
                                top: 4,
                                left: (isLanguage != true)?28:16,
                                child: Text((isLanguage != true)?"Trendi Artis Produk":"Trendy Artist Products", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),)
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
              ),
              SizedBox(height: 10,),
              GestureDetector(
                onTap: (){
                  Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: new ProductDetailActivity(isLanguage, isCurrency)) );
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 4,
                    child: Row(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width/2.6,
                          height: MediaQuery.of(context).size.height / 4,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage("assets/image/parfume_trend.png"),
                                  fit: BoxFit.cover
                              ),
                              borderRadius: BorderRadius.circular(14)
                          ),
                        ),
                        SizedBox(width: 8,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                RatingBar(
                                  itemSize: 18,
                                  initialRating: ratingCount,
                                  ignoreGestures: true,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: true,
                                  itemCount: 5,
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                                  onRatingUpdate: (rating) {
                                    print(rating);
                                  },
                                ),
                                SizedBox(width: 4,),
                                Text(ratingCount.toString(), style: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 10, color: Color(0xff872934))))
                              ],
                            ),
                            Text("[LIP] Lancome", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),)),
                            Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?300000:20), style: GoogleFonts.poppins(textStyle: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.grey, fontSize: 12),)),
                            Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?150000:10), style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 14),)),
                            SizedBox(height: 8,),
                            Container(
                                height: MediaQuery.of(context).size.height / 20,
                                width: MediaQuery.of(context).size.width / 2.8,
                                decoration: BoxDecoration(
                                    color: Color(0xffe3cea3),
                                    borderRadius: BorderRadius.circular(6.0),
                                    border: Border.all(color: Color(0xffe3cea3))
                                ),
                                child: Material(
                                  borderRadius: BorderRadius.circular(6.0),
                                  color: Colors.transparent,
                                  child: AnimatedSwitcher(
                                      duration: Duration.zero,
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: new CustomerInformationActivity(isLanguage, isCurrency)) );
                                        },
                                        splashColor: Colors.white,
                                        borderRadius: BorderRadius.circular(6.0),
                                        child: Center(
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 12.0, vertical: 4.0),
                                            child: Text((isLanguage != true)?"Beli Langsung":"Buy Now", style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 14),)),
                                          ),
                                        ),
                                      )
                                  ),
                                )
                            ),
                            SizedBox(height: 8,),
                            Container(
                                height: MediaQuery.of(context).size.height / 20,
                                width: MediaQuery.of(context).size.width / 2.8,
                                decoration: BoxDecoration(
                                    color: Color(0xff672934),
                                    borderRadius: BorderRadius.circular(6.0),
                                    border: Border.all(color: Color(0xffe3cea3))
                                ),
                                child: Material(
                                  borderRadius: BorderRadius.circular(6.0),
                                  color: Colors.transparent,
                                  child: AnimatedSwitcher(
                                      duration: Duration.zero,
                                      child: InkWell(
                                        onTap: () async{

                                        },
                                        splashColor: Colors.white,
                                        borderRadius: BorderRadius.circular(6.0),
                                        child: Center(
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 12.0, vertical: 4.0),
                                            child: Text((isLanguage != true)?"+Keranjang":"+Cart", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.white, fontSize: 14),)),
                                          ),
                                        ),
                                      )
                                  ),
                                )
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              //carousel product artist
              Container(
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: <Widget>[
                    CarouselSlider(
                      options: CarouselOptions(
                          height: MediaQuery.of(context).size.height / 4,
                          scrollDirection: Axis.horizontal,
                          enableInfiniteScroll: true,
                          onPageChanged: (index, reason){
                            setState(() {
                              _currentProduct = index;
                            });
                          }
                      ),
                      items: carouselProduct.map((e) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(e),
                                fit: BoxFit.fill,
                              )
                          ),
                        );
                      }).toList(),
                    ),
                    Positioned(
                      left: MediaQuery.of(context).size.width / 2.4,
                      bottom: 4,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: mapHeader<Widget>(carouselProduct, (index, url) {
                          return Container(
                            width: _currentProduct == index ? 12 : 10.0,
                            height: _currentProduct == index ? 12 : 10.0,
                            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: _currentProduct == index ? Color(0xff872934) : Colors.white,
                            ),
                          );
                        }),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 4,),
              //information product artist
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundImage: AssetImage("assets/image/seli_marchelia.png",
                          ),
                          radius: 18,
                        ),
                        SizedBox(width: 6,),
                        Text("Seli Marchelia", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w400,))),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Image.asset("assets/image/icon_selected_bookmark.png", width: 16, height: 16,),
                        Text("1.5k", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                        SizedBox(width: 4,),
                        Image.asset("assets/image/icon_ig.png", width: 16, height: 16,),
                        Text("10.1k", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                        SizedBox(width: 4,),
                        Image.asset("assets/image/icon_yt.png", width: 16, height: 16,),
                        Text("30.3k", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(height: 4,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Divider(thickness: 2,),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text((isLanguage != true)?"produk lipstik baru!":"New lipstick product", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Divider(thickness: 2,),
              ),
              SizedBox(height: 4,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text((descNewProduct.length > 120 && canReadMore != false)?descNewProduct.substring(0, 119)
                      :(canReadMore != true)?descNewProduct:descNewProduct,
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w400,))
                  ),
                ),
              ),
              SizedBox(height: 4,),
              (descNewProduct.length > 120 && canReadMore != false)?Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: GestureDetector(
                  onTap: (){
                    setState(() {
                      canReadMore = false;
                    });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 1.4,
                    child: Text((isLanguage != true)?"Tampilkan lebih banyak":"Read more", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, decoration: TextDecoration.underline, color: Color(0xff872934)))),
                  ),
                ),
              ):Container(),
              SizedBox(height: 8,),
              //list product artist
//                Container(
//                  width: MediaQuery.of(context).size.width,
//                  height: MediaQuery.of(context).size.height/ 8,
//                  child: ListView.builder(
//                      itemCount: 5,
//                      physics: BouncingScrollPhysics(),
//                      scrollDirection: Axis.horizontal,
//                      itemBuilder: (context, index){
//                        return CustomCard.newProductCard(context);
//                      }
//                  ),
//                ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: new ProductActivity()) );
                  },
                  child: Container(
                      width: MediaQuery.of(context).size.width / 1.4,
                      height: MediaQuery.of(context).size.height / 20,
                      decoration: BoxDecoration(
                          color: Color(0xff872934),
                          borderRadius: BorderRadius.circular(8)
                      ),
                      child: Center(
                        child: Text((isLanguage != true)?"Lebih banyak produk":"More Products", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white)),),
                      )
                  ),
                ),
              ),
              SizedBox(height: 4,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Divider(thickness: 2,),
              ),
              SizedBox(height: 4,),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 240,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 20,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 240,
                        decoration: BoxDecoration(
                            color: Color(0xfffffaf0)
                        ),
                      ),
                    ),
                    Positioned(
                      top: 10,
                      left: 20,
                      child: Container(
                        width: MediaQuery.of(context).size.width / 1.8,
                        height: 32,
                        decoration: BoxDecoration(
                            color: Color(0xff872934),
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(10), bottomRight: Radius.circular(10))
                        ),
                      ),
                    ),
                    Positioned(
                        top: 10,
                        left: (isLanguage != true)?44:34,
                        child: Text((isLanguage != true)?"Spesial Diskon":"Special Discount", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),)
                    ),
                    //list discount
                    Padding(
                      padding: const EdgeInsets.only(top: 30),
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 18),
                          child: ListView.builder(
                              itemCount: 5,
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index){
                                return CustomCard.discountCard(context, "Parfume", isCurrency);
                              }
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 8,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  width: MediaQuery.of(context).size.width / 1.8,
                  height: 32,
                  decoration: BoxDecoration(
                      color: Color(0xff872934),
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(10), bottomRight: Radius.circular(10))
                  ),
                  child: Center(
                    child: Text((isLanguage != true)?"Ulasan produk Artis":"Artist product reviews", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white)),),
                  ),
                ),
              ),
              //list Home
//                Padding(
//                  padding: const EdgeInsets.only(top: 16),
//                  child: Column(
//                    children: _tilesHome,
//                  ),
//                ),
//                Padding(
//                  padding: const EdgeInsets.only(top: 16),
//                  child: Container(
//                    child: new StaggeredGridView.count(
//                      shrinkWrap: true,
//                      physics: NeverScrollableScrollPhysics(),
//                      crossAxisCount: 4,
//                      staggeredTiles: _staggeredTilesHome,
//                      children: _tilesHome,
//                    ),
//                  ),
//                ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: ListView(
                  controller: _scrollController,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: _tilesHome,
                ),
              ),
              SizedBox(height: 20,),
            ],
          ),
        ),
      ),
    );
  }
}