import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class RegistrationActivity extends StatefulWidget {
  bool isLanguage;

  RegistrationActivity(this.isLanguage);

  @override
  _RegistrationActivityState createState() => _RegistrationActivityState(isLanguage);
}

class _RegistrationActivityState extends State<RegistrationActivity> {
  bool isLanguage;

  _RegistrationActivityState(this.isLanguage);

  TextEditingController _registrationTextPhone = TextEditingController(text: "");
  TextEditingController _registrationTextEmail = TextEditingController(text: "");
  TextEditingController _registrationTextUsername = TextEditingController(text: "");
  TextEditingController _registrationTextPassword = TextEditingController(text: "");
  FocusNode fPassword;

  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  bool isAuth = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/image/bg_auth.png"),
              fit: BoxFit.cover
          ),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              color: Colors.black.withOpacity(.8)
          ),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text((isLanguage != true)?"DAFTAR":"REGISTER", style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 32,
                              color: (isAuth != true)?Colors.white:Colors.transparent),)),
                      ),
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        height: (isAuth != true)?MediaQuery.of(context).size.height / 18:0,
                        width: (isAuth != true)?MediaQuery.of(context).size.width:0,
                        child: SingleChildScrollView(
                          physics: NeverScrollableScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text((isLanguage != true)?"Belum punya akun? silahkan daftar":"Don't have an account yet? please Registration", style: GoogleFonts.poppins(
                                textStyle: TextStyle(fontWeight: FontWeight.normal,
                                    color: (isAuth != true)?Colors.white:Colors.transparent),)),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 32,),
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        height: (isAuth != true)?MediaQuery.of(context).size.height / 3:0,
                        width: (isAuth != true)?MediaQuery.of(context).size.width:0,
                        child: SingleChildScrollView(
                          physics: NeverScrollableScrollPhysics(),
                          child: Column(
                            children: <Widget>[
                              (isAuth != true)?Container(
                                height: MediaQuery.of(context).size.height / 18,
                                child: TextField(
                                  controller: _registrationTextPhone,
                                  keyboardType: TextInputType.phone,
                                  cursorColor: (isAuth != true)?Colors.white:Colors.transparent,
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(fontSize: 14, color: Colors.white)),
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.phone, color: (isAuth != true)?Colors.white:Colors.transparent,),
                                    hintText: (isLanguage != true)?"Nomor Telepon":"Phone Number",
                                    hintStyle: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                                    helperStyle: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 14)),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: (isAuth != true)?Colors.grey:Colors.transparent, width: 2),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: (isAuth != true)?Colors.white:Colors.transparent, width: 2),
                                    ),
                                  ),
                                ),
                              ):Container(),
                              SizedBox(height: 8,),
                              (isAuth != true)?Container(
                                height: MediaQuery.of(context).size.height / 18,
                                child: TextField(
                                  controller: _registrationTextEmail,
                                  keyboardType: TextInputType.emailAddress,
                                  cursorColor: (isAuth != true)?Colors.white:Colors.transparent,
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(fontSize: 14, color: Colors.white)),
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.email, color: (isAuth != true)?Colors.white:Colors.transparent,),
                                    hintText: "Email",
                                    hintStyle: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                                    helperStyle: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 14)),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: (isAuth != true)?Colors.grey:Colors.transparent, width: 2),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: (isAuth != true)?Colors.white:Colors.transparent, width: 2),
                                    ),
                                  ),
                                ),
                              ):Container(),
                              SizedBox(height: 8,),
                              (isAuth != true)?Container(
                                height: MediaQuery.of(context).size.height / 18,
                                child: TextField(
                                  controller: _registrationTextUsername,
                                  cursorColor: (isAuth != true)?Colors.white:Colors.transparent,
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(fontSize: 14, color: Colors.white)),
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.person, color: (isAuth != true)?Colors.white:Colors.transparent,),
                                    hintText: (isLanguage != true)?"Nama Pengguna":"Username",
                                    hintStyle: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                                    helperStyle: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 14)),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: (isAuth != true)?Colors.grey:Colors.transparent, width: 2),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: (isAuth != true)?Colors.white:Colors.transparent, width: 2),
                                    ),
                                  ),
                                ),
                              ):Container(),
                              (isAuth != true)?Container(
                                height: MediaQuery.of(context).size.height / 18,
                                child: TextField(
                                  enableInteractiveSelection: false,
                                  autocorrect: false,
                                  focusNode: fPassword,
                                  onSubmitted: (term){
                                    fPassword.unfocus();
                                  },
                                  controller: _registrationTextPassword,
                                  obscureText: _obscureText,
                                  cursorColor: (isAuth != true)?Colors.white:Colors.transparent,
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(fontSize: 14, color: Colors.white)),
                                  decoration: InputDecoration(
                                      icon: Icon(Icons.lock, color: (isAuth != true)?Colors.white:Colors.transparent,),
                                      hintText: (isLanguage != true)?"Kata Sandi":"Password",
                                      hintStyle: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                                      helperStyle: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 14)),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: (isAuth != true)?Colors.grey:Colors.transparent, width: 2),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: (isAuth != true)?Colors.white:Colors.transparent, width: 2),
                                      ),
                                      suffixIcon: IconButton(
                                        onPressed: _toggle,
                                        icon: Icon(_obscureText ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off, color: (isAuth != true)?Colors.white:Colors.transparent,),
                                      )
                                  ),
                                ),
                              ):Container(),
                              SizedBox(height: 8,),
                              SizedBox(height: 20,),
                              Container(
                                  decoration: BoxDecoration(
                                    color: (isAuth != true)?Color(0xffe3cea3):Colors.transparent,
                                    borderRadius: BorderRadius.circular(6.0),
                                  ),
                                  child: Material(
                                    borderRadius: BorderRadius.circular(6.0),
                                    color: Colors.transparent,
                                    child: AnimatedSwitcher(
                                        duration: Duration.zero,
                                        child: InkWell(
                                          onTap: () async{

                                          },
                                          splashColor: Colors.white,
                                          borderRadius: BorderRadius.circular(6.0),
                                          child: Center(
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 22.0, vertical: 4.0),
                                              child: Text((isLanguage != true)?"Daftar":"Register", style: GoogleFonts.poppins(
                                                  textStyle: TextStyle(color: (isAuth != true)?Colors.black:Colors.transparent, fontSize: 18, fontWeight: FontWeight.bold)),),
                                            ),
                                          ),
                                        )
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
