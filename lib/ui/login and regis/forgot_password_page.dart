import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  TextEditingController tEmail = TextEditingController(text: "");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.width / 8),
          child: Text("Lupa password", style: GoogleFonts.poppins(
              textStyle: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff872934)))),
        ),
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
        ),
        backgroundColor: Color(0xffe3cea3),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("Kamu lupa Password?", style: GoogleFonts.poppins(
                  textStyle: TextStyle(color: Color(0xff872934), fontSize: 24, fontWeight: FontWeight.bold),)),
                SizedBox(height: 8,),
                Text("Silahkan memasukkan email registrasi Anda,", style: GoogleFonts.poppins(
                  textStyle: TextStyle(color: Color(0xff872934)),)),
                Text("Kami akan mengirimkan Anda email instruksi", style: GoogleFonts.poppins(
                  textStyle: TextStyle(color: Color(0xff872934)),)),
                Text("untuk melakukan merubah password", style: GoogleFonts.poppins(
                  textStyle: TextStyle(color: Color(0xff872934)),)),
                SizedBox(height: 40,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                    height: MediaQuery.of(context).size.height / 18,
                    child: TextField(
                      controller: tEmail,
                      keyboardType: TextInputType.emailAddress,
                      cursorColor: Color(0xff872934),
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 14)),
                      decoration: InputDecoration(
                        icon: Icon(Icons.email, color: Color(0xff872934),),
                        hintText: "Email",
                        helperStyle: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff872934), width: 2),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 40,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 60),
                  child: Container(
                      decoration: BoxDecoration(
                        color: Color(0xff872934),
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(6.0),
                        color: Colors.transparent,
                        child: AnimatedSwitcher(
                            duration: Duration.zero,
                            child: InkWell(
                              onTap: () async{
  //                              Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: HomeActivity()) );
                              },
                              splashColor: Colors.white,
                              borderRadius: BorderRadius.circular(6.0),
                              child: Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 22.0, vertical: 4.0),
                                  child: Text("Kirim Email", style: GoogleFonts.poppins(
                                      textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                                ),
                              ),
                            )
                        ),
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
