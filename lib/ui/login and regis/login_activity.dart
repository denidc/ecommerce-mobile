import 'package:ecommerce/ui/home/home_activity.dart';
import 'package:ecommerce/ui/login%20and%20regis/forgot_password_page.dart';
import 'package:ecommerce/ui/login%20and%20regis/registration_activity.dart';
import 'package:ecommerce/ui/main_activity.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:page_transition/page_transition.dart';

class LoginActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  LoginActivity(this.isLanguage, this.isCurrency);

  @override
  _LoginActivityState createState() => _LoginActivityState(isLanguage, isCurrency);
}

class _LoginActivityState extends State<LoginActivity> {
  bool isLanguage;
  bool isCurrency;

  _LoginActivityState(this.isLanguage, this.isCurrency);

  TextEditingController _loginTextPhone = TextEditingController(text: "");
  TextEditingController _loginTextPassword = TextEditingController(text: "");
  FocusNode fPassword;

  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  bool isAuth = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/image/bg_auth.png"),
            fit: BoxFit.cover
          ),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              color: Colors.black.withOpacity(.8)
          ),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: Container(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text((isLanguage != null)?"MASUK":"LOGIN", style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 32,
                              color: (isAuth != false)?Colors.white:Colors.transparent),)),
                      ),
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        height: (isAuth != false)?MediaQuery.of(context).size.height / 18:0,
                        width: (isAuth != false)?MediaQuery.of(context).size.width:0,
                        child: SingleChildScrollView(
                          physics: NeverScrollableScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                child: Text((isLanguage != null)?"Silahkan login untuk melihat lebih banyak produk favorit anda":"Please login to see more of your favorite products", style: GoogleFonts.poppins(
                                textStyle: TextStyle(fontWeight: FontWeight.normal,
                                color: (isAuth != false)?Colors.white:Colors.transparent),)),
                              ),
                            ],
                          ),
                        ),
                      ),
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        height: (isAuth != true)?MediaQuery.of(context).size.height / 18:0,
                        width: (isAuth != true)?MediaQuery.of(context).size.width:0,
                        child: SingleChildScrollView(
                          physics: NeverScrollableScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text((isLanguage != true)?"Belum punya akun?":"Don't have an account yet?", style: GoogleFonts.poppins(
                                textStyle: TextStyle(fontWeight: (isAuth != true)?FontWeight.bold:FontWeight.normal,
                                    color: (isAuth != true)?Colors.white:Colors.transparent),)),
                              Text((isLanguage != true)?"Silahkan Daftar":"Please Registration", style: GoogleFonts.poppins(
                                textStyle: TextStyle(fontWeight: (isAuth != true)?FontWeight.bold:FontWeight.normal,
                                    color: (isAuth != true)?Colors.white:Colors.transparent),)),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 32,),
//                      AnimatedContainer(
//                        duration: Duration(milliseconds: 500),
//                        height: (isAuth != false)?MediaQuery.of(context).size.height / 5.5:0,
//                        width: (isAuth != false)?MediaQuery.of(context).size.width:0,
//                        child: SingleChildScrollView(
//                          physics: NeverScrollableScrollPhysics(),
//                          child: Column(
//                            children: <Widget>[
//                              SizedBox(height: 14,),
//                              Container(
//                                  decoration: BoxDecoration(
//                                    color: Colors.black,
//                                    borderRadius: BorderRadius.circular(6.0),
//                                  ),
//                                  child: Material(
//                                    borderRadius: BorderRadius.circular(6.0),
//                                    color: Colors.transparent,
//                                    child: AnimatedSwitcher(
//                                        duration: Duration.zero,
//                                        child: InkWell(
//                                          onTap: () async{
//
//                                          },
//                                          splashColor: Colors.white,
//                                          borderRadius: BorderRadius.circular(6.0),
//                                          child: Center(
//                                            child: Padding(
//                                              padding: EdgeInsets.symmetric(
//                                                  horizontal: 22.0, vertical: 10.0),
//                                              child: Image.asset("assets/image/icon_fb.png", height: 20, width: 20,),
//                                            ),
//                                          ),
//                                        )
//                                    ),
//                                  )
//                              ),
//                              SizedBox(height: 8,),
//                              Container(
//                                decoration: BoxDecoration(
//                                  color: Colors.black,
//                                  borderRadius: BorderRadius.circular(6.0),
//                                ),
//                                child: Material(
//                                  borderRadius: BorderRadius.circular(6.0),
//                                  color: Colors.transparent,
//                                  child: AnimatedSwitcher(
//                                      duration: Duration.zero,
//                                      child: InkWell(
//                                        onTap: () async{
//
//                                        },
//                                        splashColor: Colors.white,
//                                        borderRadius: BorderRadius.circular(6.0),
//                                        child: Center(
//                                          child: Padding(
//                                            padding: EdgeInsets.symmetric(
//                                                horizontal: 22.0, vertical: 10.0),
//                                            child: Image.asset("assets/image/icon_google.png", height: 20, width: 20,),
//                                          ),
//                                        ),
//                                      )
//                                  ),
//                                ),
//                              ),
//                              SizedBox(height: 16,),
//                              Padding(
//                                padding: const EdgeInsets.symmetric(horizontal: 50),
//                                child: Image.asset("assets/image/icon_line.png"),
//                              ),
//                            ],
//                          ),
//                        ),
//                      ),
//                      SizedBox(height: 8,),
//                      Row(
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//                          GestureDetector(
//                            onTap: (){
//                              FocusScope.of(context).requestFocus(FocusNode());
//                              setState(() {
//                                isAuth = true;
//                              });
//                            },
//                            child: Container(
//                                child: Text("LOGIN",
//                                style: GoogleFonts.poppins(
//                                  textStyle: TextStyle(decoration: TextDecoration.underline, fontWeight: (isAuth != false)?FontWeight.bold:FontWeight.normal,
//                                      fontSize: (isAuth != false)?18:16), color: Colors.white))),
//                          ),
//                          Container(
//                            height: 30,
//                              child: VerticalDivider(thickness: 2, color: Colors.white,)),
//                          GestureDetector(
//                            onTap: (){
//                              FocusScope.of(context).requestFocus(FocusNode());
//                              setState(() {
//                                isAuth = false;
//                              });
//                            },
//                            child: Container(
//                                child: Text("SIGN UP",
//                                style: GoogleFonts.poppins(
//                                  textStyle: TextStyle(decoration: TextDecoration.underline, fontWeight: (isAuth != false)?FontWeight.normal:FontWeight.bold,
//                                      fontSize: (isAuth != false)?16:18), color: Colors.white))),
//                          )
//                        ],
//                      ),
                      SizedBox(height: 8,),
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        height: (isAuth != false)?MediaQuery.of(context).size.height / 4.2:0,
                        width: (isAuth != false)?MediaQuery.of(context).size.width:0,
                        child: SingleChildScrollView(
                          physics: NeverScrollableScrollPhysics(),
                          child: Column(
                            children: <Widget>[
                              (isAuth != false)?Container(
                                height: MediaQuery.of(context).size.height / 18,
                                child: TextField(
                                  controller: _loginTextPhone,
                                  keyboardType: TextInputType.phone,
                                  cursorColor: (isAuth != false)?Colors.white:Colors.transparent,
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(fontSize: 14, color: Colors.white)),
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.phone, color: (isAuth != false)?Colors.white:Colors.transparent,),
                                    hintText: (isLanguage != true)?"Nomor Telepon":"Phone Number",
                                    hintStyle: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                                    helperStyle: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 14)),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: (isAuth != false)?Colors.grey:Colors.transparent, width: 2),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: (isAuth != false)?Colors.white:Colors.transparent, width: 2),
                                    ),
                                  ),
                                ),
                              ):Container(),
                              SizedBox(height: 8,),
                              (isAuth != false)?Container(
                                height: MediaQuery.of(context).size.height / 18,
                                child: TextField(
                                  enableInteractiveSelection: false,
                                  autocorrect: false,
                                  focusNode: fPassword,
                                  onSubmitted: (term){
                                    fPassword.unfocus();
                                  },
                                  obscureText: _obscureText,
                                  controller: _loginTextPassword,
                                  cursorColor: (isAuth != false)?Colors.white:Colors.transparent,
                                  style: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 14, color: Colors.white)),
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.lock, color: (isAuth != false)?Colors.white:Colors.transparent,),
                                      hintText: (isLanguage != true)?"Kata Sandi":"Password",
                                      hintStyle: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12, color: Colors.grey)),
                                      helperStyle: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 14)),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: (isAuth != false)?Colors.grey:Colors.transparent, width: 2),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: (isAuth != false)?Colors.white:Colors.transparent, width: 2),
                                    ),
                                      suffixIcon: IconButton(
                                        onPressed: _toggle,
                                        icon: Icon(_obscureText ? MaterialCommunityIcons.eye : MaterialCommunityIcons.eye_off,
                                          color: (isAuth != false)?Colors.white:Colors.transparent,),
                                      )
                                  ),
                                ),
                              ):Container(),
                              SizedBox(height: 4,),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ForgotPasswordPage()) );
                                  },
                                  child: Container(
                                    child: Text((isLanguage != true)?"Lupa Kata Sandi":"Forgot password", style: GoogleFonts.poppins(
                                        textStyle: TextStyle(fontSize: 12, decoration: TextDecoration.underline,
                                            color: (isAuth != false)?Colors.white:Colors.transparent)),),
                                  ),
                                ),
                              ),
                              SizedBox(height: 30,),
                              Container(
                                  decoration: BoxDecoration(
                                    color: (isAuth != false)?Color(0xffe3cea3):Colors.transparent,
                                    borderRadius: BorderRadius.circular(6.0),
                                  ),
                                  child: Material(
                                    borderRadius: BorderRadius.circular(6.0),
                                    color: Colors.transparent,
                                    child: AnimatedSwitcher(
                                        duration: Duration.zero,
                                        child: InkWell(
                                          onTap: () async{
                                            Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: MainActivity(isLanguage, isCurrency, false)) );
                                          },
                                          splashColor: Colors.white,
                                          borderRadius: BorderRadius.circular(6.0),
                                          child: Center(
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 22.0, vertical: 4.0),
                                              child: Text((isLanguage != true)?"Masuk":"Login", style: GoogleFonts.poppins(
                                                  textStyle: TextStyle(color: (isAuth != false)?Colors.black:Colors.transparent, fontSize: 18, fontWeight: FontWeight.bold)),),
                                            ),
                                          ),
                                        )
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 12,),
                      Text((isLanguage != true)?"Belum punya akun?":"Don't have an account yet?", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontWeight: FontWeight.normal,
                            color: (isAuth != false)?Colors.white:Colors.transparent),)),
                      GestureDetector(
                        onTap: (){
                          FocusScope.of(context).requestFocus(FocusNode());
                          Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: RegistrationActivity(isLanguage)) );
                        },
                        child: Text((isLanguage != true)?"daftar sekarang":"registraton now", style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontWeight: FontWeight.bold, decoration: TextDecoration.underline,
                              color: (isAuth != false)?Colors.white:Colors.transparent),)),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
