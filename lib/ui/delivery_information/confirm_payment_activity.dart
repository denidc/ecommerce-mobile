import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class ConfirmPaymentActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  ConfirmPaymentActivity(this.isLanguage, this.isCurrency);

  @override
  _ConfirmPaymentActivityState createState() => _ConfirmPaymentActivityState(isLanguage, isCurrency);
}

class _ConfirmPaymentActivityState extends State<ConfirmPaymentActivity> {
  bool isLanguage;
  bool isCurrency;

  _ConfirmPaymentActivityState(this.isLanguage, this.isCurrency);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 16,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Column(
                      children: [
                        Text((isLanguage != true)?"Informasi":"Information", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold)),),
                        Text((isLanguage != true)?"Pelanggan":"Customer", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold)),),
                        SizedBox(height: 4,),
                        Container(
                          width: MediaQuery.of(context).size.width / 8,
                          height: MediaQuery.of(context).size.height / 32,
                          decoration: BoxDecoration(
                              color: Color(0xffe3cea3),
                              shape: BoxShape.circle
                          ),
                          child: Icon(Icons.check, color: Colors.white,),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: Container(
                        width: MediaQuery.of(context).size.width / 4,
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Text((isLanguage != true)?"Pembayaran":"Payment", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold)),),
                        SizedBox(height: 4,),
                        Container(
                          width: MediaQuery.of(context).size.width / 8,
                          height: MediaQuery.of(context).size.height / 32,
                          decoration: BoxDecoration(
                              color: Color(0xffe3cea3),
                              shape: BoxShape.circle
                          ),
                          child: Icon(Icons.check, color: Colors.white,),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: Container(
                        width: MediaQuery.of(context).size.width / 4,
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Text((isLanguage != true)?"Ulasan":"Review", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold)),),
                        Text((isLanguage != true)?"& Bayar":"& Pay", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold)),),
                        SizedBox(height: 4,),
                        Container(
                          width: MediaQuery.of(context).size.width / 8,
                          height: MediaQuery.of(context).size.height / 32,
                          decoration: BoxDecoration(
                              color: Colors.black,
                              shape: BoxShape.circle
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 24,),
                Row(
                  children: [
                    Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                        shape: BoxShape.circle
                      ),
                      child: Icon(Icons.check),
                    ),
                    SizedBox(width: 8,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text((isLanguage != true)?"Pesanan #172354":"Order #172354", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),),
                        Text((isLanguage != true)?"Terimakasih Seli":"Thank You Seli", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 16,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Informasi Pengirim":"Sender Information", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),),
                    Text("Edit", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, decoration: TextDecoration.underline)),)
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Seli Marchelia", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Text("selimar15@gmail.com", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),)
                  ],
                ),
                Text("Jl. Jemursari, 60118", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 14)),),
                Text("Surabaya, Indonesia", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 14)),),
                Text("0823724374623", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 14)),),
                SizedBox(height: 16,),
                Divider(thickness: 2,),
                SizedBox(height: 16,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Metode Pengiriman":"Delivery Method", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),),
                    Text("Edit", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, decoration: TextDecoration.underline)),)
                  ],
                ),
                SizedBox(height: 8,),
                Text("SICEPAT REG (2-3 HARI)", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 16)),),
                SizedBox(height: 16,),
                Divider(thickness: 2,),
                SizedBox(height: 16,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Metode Pembayaran":"Payment Method", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),),
                    Text("Edit", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, decoration: TextDecoration.underline)),)
                  ],
                ),
                SizedBox(height: 8,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("xxxx xxxx xxxx 6034", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Text((isLanguage != true)?"Total Pembayaran":"Total Payment", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("VISA Credit Card", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?494000:33), style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),)
                  ],
                ),
                SizedBox(height: 16,),
                Divider(thickness: 2,),
                SizedBox(height: 16,),
                Text((isLanguage != true)?"Keranjang Belanja":"Shipping Cart", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),),
                SizedBox(height: 8,),
                Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Container(
                              width: 70,
                              height: 70,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage("assets/image/parfume_sakura.png"),
                                      fit: BoxFit.cover
                                  )
                              ),
                            ),
                            SizedBox(width: 8,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Lanaige", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 14)),),
                                Row(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          border: Border.all(color: Colors.grey)
                                      ),
                                      child: Center(
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 6),
                                            child: Text("S", style: GoogleFonts.poppins(
                                                textStyle: TextStyle(fontSize: 12)),),
                                          )),
                                    ),
                                    SizedBox(width: 4,),
                                    Container(
                                      width: 20,
                                      height: 20,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: Colors.black
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 8,),
                                Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?249000:16), style: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),)
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 4,),
                      Container(
                        child: Row(
                          children: [
                            Container(
                              width: 70,
                              height: 70,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage("assets/image/parfume_sakura.png"),
                                      fit: BoxFit.cover
                                  )
                              ),
                            ),
                            SizedBox(width: 8,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Lanaige", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 14)),),
                                Row(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          border: Border.all(color: Colors.grey)
                                      ),
                                      child: Center(
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 6),
                                            child: Text("S", style: GoogleFonts.poppins(
                                                textStyle: TextStyle(fontSize: 12)),),
                                          )),
                                    ),
                                    SizedBox(width: 4,),
                                    Container(
                                      width: 20,
                                      height: 20,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: Colors.black
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 8,),
                                Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?249000:16), style: GoogleFonts.poppins(
                                    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),)
                              ],
                            ),
                          ],
                        ),
                      ),
                    ]
                ),
                SizedBox(height: 18,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Jumlah":"Total", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?475000:32), style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Ongkos Kirim":"Postal fee", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?19000:1.3), style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ],
                ),
                Divider(thickness: 2,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Jumlah Harga":"Total Price", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?494000:33), style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 18,),
                Container(
                    decoration: BoxDecoration(
                      color: Color(0xff872934),
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: Colors.transparent,
                      child: AnimatedSwitcher(
                          duration: Duration.zero,
                          child: InkWell(
                            onTap: () {

                            },
                            splashColor: Colors.white,
                            borderRadius: BorderRadius.circular(6.0),
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 22.0, vertical: 4.0),
                                child: Text((isLanguage != true)?"Konfirmasi dan Bayar":"Confirmation and Pay", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                              ),
                            ),
                          )
                      ),
                    )
                ),
                SizedBox(height: 18,),
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.transparent
                    ),
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
                        SizedBox(width: MediaQuery.of(context).size.height / 8,),
                        Text((isLanguage != true)?"Kembali":"Back", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20,)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
