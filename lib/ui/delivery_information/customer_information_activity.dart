import 'package:ecommerce/ui/delivery_information/payment_activity.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';

class CustomerInformationActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  CustomerInformationActivity(this.isLanguage, this.isCurrency);

  @override
  _CustomerInformationActivityState createState() => _CustomerInformationActivityState(isLanguage, isCurrency);
}

class _CustomerInformationActivityState extends State<CustomerInformationActivity> {
  bool isLanguage;
  bool isCurrency;

  _CustomerInformationActivityState(this.isLanguage, this.isCurrency);

  TextEditingController emailController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController streetNameController = TextEditingController();
  TextEditingController subDistrictController = TextEditingController();
  TextEditingController districtController = TextEditingController();
  TextEditingController provinceController = TextEditingController();
  TextEditingController postalCodeController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController noTelephoneController = TextEditingController();
  TextEditingController couponController = TextEditingController();
  bool checkBoxNotificationPromo = false;
  bool checkBoxInformation = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 16,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Column(
                      children: [
                        Text((isLanguage != true)?"Informasi":"Information", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold)),),
                        Text((isLanguage != true)?"Pelanggan":"Customer", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold)),),
                        SizedBox(height: 4,),
                        Container(
                          width: MediaQuery.of(context).size.width / 8,
                          height: MediaQuery.of(context).size.height / 32,
                          decoration: BoxDecoration(
                            color: Colors.black,
                            shape: BoxShape.circle
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: Container(
                        width: MediaQuery.of(context).size.width / 4,
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Text((isLanguage != true)?"Pembayaran":"Payment", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold, color: Colors.grey)),),
                        SizedBox(height: 4,),
                        Container(
                          width: MediaQuery.of(context).size.width / 8,
                          height: MediaQuery.of(context).size.height / 32,
                          decoration: BoxDecoration(
                              color: Colors.grey,
                              shape: BoxShape.circle
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: Container(
                        width: MediaQuery.of(context).size.width / 4,
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Text((isLanguage != true)?"Ulasan":"Review", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold, color: Colors.grey)),),
                        Text((isLanguage != true)?"& Bayar":"& Pay", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold, color: Colors.grey)),),
                        SizedBox(height: 4,),
                        Container(
                          width: MediaQuery.of(context).size.width / 8,
                          height: MediaQuery.of(context).size.height / 32,
                          decoration: BoxDecoration(
                              color: Colors.grey,
                              shape: BoxShape.circle
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 24,),
                Text((isLanguage != true)?"Informasi Pelanggan":"Information Customer", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),),
                Row(
                  children: [
                    Text((isLanguage != true)?"Sudah Punya Akun? ":"Already have account?", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 12)),),
                    GestureDetector(
                      onTap: (){

                      },
                        child: Text((isLanguage != true)?" Masuk":" Login", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12, decoration: TextDecoration.underline)),)
                    ),
                  ],
                ),
                SizedBox(height: 8,),
                TextField(
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: "Email",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                  ),
                  controller: emailController,
                ),
                Row(
                  children: [
                    Checkbox(
                        value: checkBoxNotificationPromo,
                        activeColor: Color(0xffe3cea3),
                        onChanged:(bool newValue){
                          setState(() {
                            checkBoxNotificationPromo = newValue;
                          });
                        }),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text((isLanguage != true)?"Beritahu saya jika ada tawaran spesial":"Let me know if there are any specials", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12)),),
                        Text((isLanguage != true)?"dan promo khusus":"and special promos", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12)),),
                      ],
                    )
                  ],
                ),
                SizedBox(height: 12,),
                Text((isLanguage != true)?"Alamat Pengiriman":"Shipping Address", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),),
                SizedBox(height: 4,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 2.25,
                      child: TextField(
                        cursorColor: Colors.grey,
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 12),
                          hintText: (isLanguage != true)?"Nama Depan":"First Name",
                          hintStyle: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                        ),
                        controller: firstNameController,
                      ),
                    ),
                    SizedBox(width: 8,),
                    Container(
                      width: MediaQuery.of(context).size.width / 2.25,
                      child: TextField(
                        cursorColor: Colors.grey,
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 12),
                          hintText: (isLanguage != true)?"Nama Belakang":"Last Name",
                          hintStyle: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                        ),
                        controller: lastNameController,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8,),
                TextField(
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: (isLanguage != true)?"Nama Jalan, Nomor Rumah, RT/RW":"Street Name, Address Number, RT/RW",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                  ),
                  controller: streetNameController,
                ),
                SizedBox(height: 8,),
                TextField(
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: (isLanguage != true)?"Kecamatan":"Sub District",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                  ),
                  controller: subDistrictController,
                ),
                SizedBox(height: 8,),
                TextField(
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: (isLanguage != true)?"Kabupaten":"District",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                  ),
                  controller: districtController,
                ),
                SizedBox(height: 8,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 2.25,
                      child: TextField(
                        cursorColor: Colors.grey,
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 12),
                          hintText: (isLanguage != true)?"Provinsi":"Province",
                          hintStyle: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                        ),
                        controller: provinceController,
                      ),
                    ),
                    SizedBox(width: 8,),
                    Container(
                      width: MediaQuery.of(context).size.width / 2.25,
                      child: TextField(
                        cursorColor: Colors.grey,
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14)),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 12),
                          hintText: (isLanguage != true)?"Kode Pos":"Postal Code",
                          hintStyle: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                        ),
                        controller: postalCodeController,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8,),
                TextField(
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: (isLanguage != true)?"Negara":"Country",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                  ),
                  controller: countryController,
                ),
                SizedBox(height: 8,),
                TextField(
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: (isLanguage != true)?"Nomer HP":"Phone Number",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                  ),
                  controller: noTelephoneController,
                ),
                Row(
                  children: [
                    Checkbox(
                        value: checkBoxInformation,
                        activeColor: Color(0xffe3cea3),
                        onChanged:(bool newValue){
                          setState(() {
                            checkBoxInformation = newValue;
                          });
                        }),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text((isLanguage != true)?"Simpan informasi ini untuk pemesanan":"Save this information for", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12)),),
                        Text((isLanguage != true)?"selanjutnya":"future orders", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 12)),),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 18,),
                Text((isLanguage != true)?"Keranjang Belanja":"Shopping cart", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),),
                SizedBox(height: 8,),
                Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("assets/image/parfume_sakura.png"),
                                fit: BoxFit.cover
                              )
                            ),
                          ),
                          SizedBox(width: 8,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Lanaige", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 14)),),
                              Row(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      border: Border.all(color: Colors.grey)
                                    ),
                                    child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 6),
                                          child: Text("S", style: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12)),),
                                        )),
                                  ),
                                  SizedBox(width: 4,),
                                  Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                      color: Colors.black
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 8,),
                              Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?249000:16), style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),)
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 4,),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("assets/image/parfume_sakura.png"),
                                fit: BoxFit.cover
                              )
                            ),
                          ),
                          SizedBox(width: 8,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Lanaige", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 14)),),
                              Row(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      border: Border.all(color: Colors.grey)
                                    ),
                                    child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 6),
                                          child: Text("S", style: GoogleFonts.poppins(
                                          textStyle: TextStyle(fontSize: 12)),),
                                        )),
                                  ),
                                  SizedBox(width: 4,),
                                  Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                      color: Colors.black
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 8,),
                              Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?249000:16), style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),)
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16,),
                TextField(
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: (isLanguage != true)?"Gunakan Kupon":"Use Coupons",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                  ),
                  controller: couponController,
                ),
                SizedBox(height: 8,),
                Container(
                    decoration: BoxDecoration(
                      color: Color(0xff872934),
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: Colors.transparent,
                      child: AnimatedSwitcher(
                          duration: Duration.zero,
                          child: InkWell(
                            onTap: () async{

                            },
                            splashColor: Colors.white,
                            borderRadius: BorderRadius.circular(6.0),
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 22.0, vertical: 4.0),
                                child: Text((isLanguage != true)?"Cek":"Check", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                              ),
                            ),
                          )
                      ),
                    )
                ),
                SizedBox(height: 18,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Jumlah":"Total", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?475000:32), style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Ongkos Kirim":"Postal Fee", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("-", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ],
                ),
                Divider(thickness: 2,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Jumlah Harga":"Total Price", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?475000:32), style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 18,),
                Container(
                    decoration: BoxDecoration(
                      color: Color(0xff872934),
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: Colors.transparent,
                      child: AnimatedSwitcher(
                          duration: Duration.zero,
                          child: InkWell(
                            onTap: () {
                              Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: new PaymentActivity(isLanguage, isCurrency)) );
                            },
                            splashColor: Colors.white,
                            borderRadius: BorderRadius.circular(6.0),
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 22.0, vertical: 4.0),
                                child: Text((isLanguage != true)?"Lanjut ke Pengiriman":"Proceed to Delivery", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                              ),
                            ),
                          )
                      ),
                    )
                ),
                SizedBox(height: 18,),
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.transparent
                    ),
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
                        SizedBox(width: MediaQuery.of(context).size.height / 8,),
                        Text((isLanguage != true)?"Kembali":"Back", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20,)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
