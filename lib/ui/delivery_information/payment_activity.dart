import 'package:ecommerce/ui/delivery_information/confirm_payment_activity.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';

class PaymentActivity extends StatefulWidget {
  bool isLanguage;
  bool isCurrency;

  PaymentActivity(this.isLanguage, this.isCurrency);

  @override
  _PaymentActivityState createState() => _PaymentActivityState(isLanguage, isCurrency);
}

class _PaymentActivityState extends State<PaymentActivity> {
  bool isLanguage;
  bool isCurrency;

  _PaymentActivityState(this.isLanguage, this.isCurrency);

  List<RadioModelDelivery> sampleDataDelivery = new List<RadioModelDelivery>();
  List<RadioModelPaymentMethod> sampleDataPaymentMethod = new List<RadioModelPaymentMethod>();
  ScrollController _scrollController;
  TextEditingController couponController = TextEditingController();

  @override
  void initState() {
    sampleDataDelivery.add(new RadioModelDelivery(0,false, 'SICEPAT (2-3 HARI)', NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?19000:1.3)));
    sampleDataDelivery.add(new RadioModelDelivery(1,false, 'JNE EXPRESS (1 HARI)', NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: (isCurrency != true)?0:1).format((isCurrency != true)?27000:1.8)));
    sampleDataPaymentMethod.add(new RadioModelPaymentMethod(0,false, 'Online Payment'));
    sampleDataPaymentMethod.add(new RadioModelPaymentMethod(1,false, 'Via Transfer Bank'));
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 16,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Column(
                      children: [
                        Text((isLanguage != true)?"Informasi":"Information", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold)),),
                        Text((isLanguage != true)?"Pelanggan":"Customer", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold)),),
                        SizedBox(height: 4,),
                        Container(
                          width: MediaQuery.of(context).size.width / 8,
                          height: MediaQuery.of(context).size.height / 32,
                          decoration: BoxDecoration(
                              color: Color(0xffe3cea3),
                              shape: BoxShape.circle
                          ),
                          child: Icon(Icons.check, color: Colors.white,),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: Container(
                        width: MediaQuery.of(context).size.width / 4,
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Text((isLanguage != true)?"Pembayaran":"Payment", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold, color: Colors.black)),),
                        SizedBox(height: 4,),
                        Container(
                          width: MediaQuery.of(context).size.width / 8,
                          height: MediaQuery.of(context).size.height / 32,
                          decoration: BoxDecoration(
                              color: Colors.black,
                              shape: BoxShape.circle
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: Container(
                        width: MediaQuery.of(context).size.width / 4,
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Text((isLanguage != true)?"Ulasan":"Review", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold, color: Colors.grey)),),
                        Text((isLanguage != true)?"& Bayar":"& Pay", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 8, fontWeight: FontWeight.bold, color: Colors.grey)),),
                        SizedBox(height: 4,),
                        Container(
                          width: MediaQuery.of(context).size.width / 8,
                          height: MediaQuery.of(context).size.height / 32,
                          decoration: BoxDecoration(
                              color: Colors.grey,
                              shape: BoxShape.circle
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 24,),
                Text((isLanguage != true)?"Pengiriman":"Delivery", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),),
                SizedBox(height: 4,),
                Text((isLanguage != true)?"Pilih Metode Pengiriman sesuai Kebutuhan":"Select The Shipping Method as Needed", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 12)),),
                SizedBox(height: 12,),
                new ListView.builder(
                  controller: _scrollController,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: sampleDataDelivery.length,
                  itemBuilder: (BuildContext context, int index) {
                    return new InkWell(
                      highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          sampleDataDelivery.forEach((element) => element.isSelected = false);
                          sampleDataDelivery[index].isSelected = true;

                        });
                      },
                      child: new RadioItemDelivery(sampleDataDelivery[index]),
                    );
                  },
                ),
                SizedBox(height: 14,),
                Text((isLanguage != true)?"Metode Pembayaran":"Payment Method", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),),
                SizedBox(height: 4,),
                Text((isLanguage != true)?"Silahkan Pilih Metode Pembayaran Anda":"Please Select Your Payment Method", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 12)),),
                SizedBox(height: 4,),
                Text((isLanguage != true)?"(Semua Transaksi Aman & Bersifat Rahasia)":"(All Transactions are Safe & Confidential)", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 12)),),
                SizedBox(height: 12,),
                new ListView.builder(
                  controller: _scrollController,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: sampleDataPaymentMethod.length,
                  itemBuilder: (BuildContext context, int index) {
                    return new InkWell(
                      highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          sampleDataPaymentMethod.forEach((element) => element.isSelected = false);
                          sampleDataPaymentMethod[index].isSelected = true;

                        });
                      },
                      child: new RadioItemPaymentMethod(sampleDataPaymentMethod[index]),
                    );
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Informasi Pengirim":"Sender Information", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),),
                    Text("Edit", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, decoration: TextDecoration.underline)),)
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Seli Marchelia", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Text("selimar15@gmail.com", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),)
                  ],
                ),
                Text("Jl. Jemursari, 60118", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 14)),),
                Text("Surabaya, Indonesia", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 14)),),
                Text("0823724374623", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 14)),),
                SizedBox(height: 16,),
                Text((isLanguage != true)?"Keranjang Belanja":"Shipping Cart", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),),
                SizedBox(height: 8,),
                Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage("assets/image/parfume_sakura.png"),
                                    fit: BoxFit.cover
                                )
                            ),
                          ),
                          SizedBox(width: 8,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Lanaige", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 14)),),
                              Row(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        border: Border.all(color: Colors.grey)
                                    ),
                                    child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 6),
                                          child: Text("S", style: GoogleFonts.poppins(
                                              textStyle: TextStyle(fontSize: 12)),),
                                        )),
                                  ),
                                  SizedBox(width: 4,),
                                  Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        color: Colors.black
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 8,),
                              Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?249000:16), style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),)
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 4,),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage("assets/image/parfume_sakura.png"),
                                    fit: BoxFit.cover
                                )
                            ),
                          ),
                          SizedBox(width: 8,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Lanaige", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 14)),),
                              Row(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        border: Border.all(color: Colors.grey)
                                    ),
                                    child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 6),
                                          child: Text("S", style: GoogleFonts.poppins(
                                              textStyle: TextStyle(fontSize: 12)),),
                                        )),
                                  ),
                                  SizedBox(width: 4,),
                                  Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        color: Colors.black
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 8,),
                              Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?249000:16), style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),)
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16,),
                TextField(
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: (isLanguage != true)?"Gunakan Kupon":"Use Coupons",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                  ),
                  controller: couponController,
                ),
                SizedBox(height: 8,),
                Container(
                    decoration: BoxDecoration(
                      color: Color(0xff872934),
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: Colors.transparent,
                      child: AnimatedSwitcher(
                          duration: Duration.zero,
                          child: InkWell(
                            onTap: () async{

                            },
                            splashColor: Colors.white,
                            borderRadius: BorderRadius.circular(6.0),
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 22.0, vertical: 4.0),
                                child: Text((isLanguage != true)?"Cek":"Check", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                              ),
                            ),
                          )
                      ),
                    )
                ),
                SizedBox(height: 18,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Jumlah":"Total", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?475000:32), style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Ongkos Kirim":"Postal Fee", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("-", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ],
                ),
                Divider(thickness: 2,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text((isLanguage != true)?"Jumlah Harga":"Total Price", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?475000:32), style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 18,),
                Container(
                    decoration: BoxDecoration(
                      color: Color(0xff872934),
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: Colors.transparent,
                      child: AnimatedSwitcher(
                          duration: Duration.zero,
                          child: InkWell(
                            onTap: () async{
                              Navigator.push(context, PageTransition(type: PageTransitionType.fade, child: new ConfirmPaymentActivity(isLanguage, isCurrency)) );
                            },
                            splashColor: Colors.white,
                            borderRadius: BorderRadius.circular(6.0),
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 22.0, vertical: 4.0),
                                child: Text((isLanguage != true)?"Proses Sekarang":"Process Now", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),),
                              ),
                            ),
                          )
                      ),
                    )
                ),
                SizedBox(height: 18,),
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.transparent
                    ),
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, color: Color(0xff872934)),
                        SizedBox(width: MediaQuery.of(context).size.height / 8,),
                        Text((isLanguage != true)?"Kembali":"Back", style: GoogleFonts.poppins(
                            textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),)
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20,)
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class RadioItemDelivery extends StatefulWidget {
  final RadioModelDelivery _item;

  RadioItemDelivery(this._item);

  @override
  _RadioItemDeliveryState createState() => _RadioItemDeliveryState(_item);
}

class _RadioItemDeliveryState extends State<RadioItemDelivery> {
  RadioModelDelivery _item;

  _RadioItemDeliveryState(this._item);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  _item.isSelected ? new Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: new Icon(Icons.check_box),
                  ) : new Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: new Icon(Icons.check_box_outline_blank),
                  ),
                  new Container(
                    margin: new EdgeInsets.only(top: _item.isSelected?0:2, bottom: _item.isSelected?0:2),
                    child: new Text(_item.text, style: GoogleFonts.poppins(
                        textStyle: TextStyle(color: _item.isSelected?Colors.black:Colors.grey, fontSize: 12))),
                  ),
                ],
              ),
              Text(_item.price, style: GoogleFonts.poppins(
                  textStyle: TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.bold)))
            ],
          ),
          SizedBox(height: 8,)
        ],
      ),
    );
  }
}

class RadioItemPaymentMethod extends StatefulWidget {
  final RadioModelPaymentMethod _item;

  RadioItemPaymentMethod(this._item);

  @override
  _RadioItemPaymentMethodState createState() => _RadioItemPaymentMethodState(_item);
}

class _RadioItemPaymentMethodState extends State<RadioItemPaymentMethod> {
  RadioModelPaymentMethod _item;

  _RadioItemPaymentMethodState(this._item);

  TextEditingController ccNumberController = TextEditingController();
  TextEditingController cardNameController = TextEditingController();
  TextEditingController cvvController = TextEditingController();
  TextEditingController expiryDateController = TextEditingController();
  bool isSelected = false;
  bool checkBoxSaveInformation = false;

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  _item.isSelected ? new Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: new Icon(Icons.check_box),
                  ) : new Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: new Icon(Icons.check_box_outline_blank),
                  ),
                  new Container(
                    margin: new EdgeInsets.only(top: _item.isSelected?0:2, bottom: _item.isSelected?0:2),
                    child: new Text(_item.text, style: GoogleFonts.poppins(
                        textStyle: TextStyle(color: _item.isSelected?Colors.black:Colors.grey, fontSize: 12))),
                  ),
                ],
              ),
              IconButton(
                onPressed: (){
                  setState(() {
                    isSelected = !isSelected;
                  });
                },
                  icon: (isSelected)? Icon(Icons.keyboard_arrow_down) : Icon(Icons.arrow_forward_ios)
              ),
            ],
          ),
          (isSelected)?TextField(
            cursorColor: Colors.grey,
            style: GoogleFonts.poppins(
                textStyle: TextStyle(fontSize: 14)),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 12),
              hintText: "Credit Card Numer",
              hintStyle: GoogleFonts.poppins(
                  textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.grey, width: 2),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.grey, width: 2),
              ),
            ),
            controller: ccNumberController,
          ):Container(),
          SizedBox(height: 4,),
          (isSelected)?TextField(
            cursorColor: Colors.grey,
            style: GoogleFonts.poppins(
                textStyle: TextStyle(fontSize: 14)),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 12),
              hintText: "Name of Card",
              hintStyle: GoogleFonts.poppins(
                  textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.grey, width: 2),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: Colors.grey, width: 2),
              ),
            ),
            controller: cardNameController,
          ):Container(),
          SizedBox(height: 4,),
          (isSelected)?Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 4,
                child: TextField(
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: "CVV",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                  ),
                  controller: cvvController,
                ),
              ),
              SizedBox(width: 4,),
              Container(
                width: MediaQuery.of(context).size.width / 1.55,
                child: TextField(
                  cursorColor: Colors.grey,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14)),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 12),
                    hintText: "Expiry Date",
                    hintStyle: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 14, color: Colors.grey)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                  ),
                  controller: expiryDateController,
                ),
              ),
            ],
          ):Container(),
          SizedBox(height: 4,),
          (isSelected)?Row(
            children: [
              Checkbox(
                  value: checkBoxSaveInformation,
                  activeColor: Color(0xffe3cea3),
                  onChanged:(bool newValue){
                    setState(() {
                      checkBoxSaveInformation = newValue;
                    });
                  }),
              Text("Simpan Informasi ini untuk Berikutnya", style: GoogleFonts.poppins(
                  textStyle: TextStyle(fontSize: 12)),),
            ],
          ):Container(),
        ],
      ),
    );
  }
}

class RadioModelDelivery {
  int id;
  bool isSelected;
  final String text;
  final String price;

  RadioModelDelivery(this.id, this.isSelected, this.text, this.price);
}

class RadioModelPaymentMethod {
  int id;
  bool isSelected;
  final String text;

  RadioModelPaymentMethod(this.id, this.isSelected, this.text);
}
