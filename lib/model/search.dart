class Search {
  final int userId;
  final int id;
  final String title;
  final String body;

  Search({this.userId, this.id, this.title, this.body});

  factory Search.formJson(Map <String, dynamic> json){
    return new Search(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      body: json['body'],
    );
  }
}