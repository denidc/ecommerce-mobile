import 'dart:io';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';

class FullImage extends StatefulWidget {
  String url;
  FullImage(this.url);

  @override
  _FullImageState createState() => _FullImageState(url);
}

class _FullImageState extends State<FullImage> {
  String url;
  _FullImageState(this.url);

  Future download(String url) async{
    Directory tempDir = await getExternalStorageDirectory();
    String tempPath = tempDir.path;

    String name = url.split('?')[0].toString();
    String fixName = name.split('/').last.toString();
    await FlutterDownloader.enqueue(
      url: url,
      savedDir: "/storage/emulated/0/Download/",
      showNotification: true,
      openFileFromNotification: true,
      fileName: fixName + ".png",
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            actions: <Widget>[
              IconButton(
                onPressed: (){
                  download(url);
                },
                icon: Icon(Icons.file_download),
              )
            ],
          ),
          body: Container(
            child: PhotoView(
              imageProvider: NetworkImage(url),
              minScale: PhotoViewComputedScale.contained * 1.0,
              maxScale: PhotoViewComputedScale.covered * 1.8,
            ),
          ),
        ));
  }
}
