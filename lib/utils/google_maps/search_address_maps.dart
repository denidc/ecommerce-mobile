import 'dart:async';

import 'file:///C:/Users/user/AndroidStudioProjects/ecommerce/lib/ui/setting/user/shipping_address_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:page_transition/page_transition.dart';

class SearchAddressMaps extends StatefulWidget {
  bool isLanguage;
  Position currentPosition;

  SearchAddressMaps(this.isLanguage, this.currentPosition);

  @override
  _SearchAddressMapsState createState() => _SearchAddressMapsState(isLanguage, currentPosition);
}

class _SearchAddressMapsState extends State<SearchAddressMaps> {
  bool isLanguage;
  Position currentPosition;

  _SearchAddressMapsState(this.isLanguage, this.currentPosition);

  String address = "";
  var geolocator = Geolocator();
  double _lat;
  double _long;
  GoogleMapController mapController;
  Completer<GoogleMapController> _controller = Completer();
  bool isMove = false;

  @override
  void initState() {
    setState(() {
      _lat = currentPosition.latitude;
      _long = currentPosition.longitude;
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                height: double.infinity,
                child: GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(currentPosition.latitude, currentPosition.longitude),
                    zoom: 18,
                  ),
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                  myLocationEnabled: true,
                  compassEnabled: true,
                  myLocationButtonEnabled: true,
                  mapToolbarEnabled: true,
                  onCameraIdle: () async{
                    var addresses = await Geocoder.local.findAddressesFromCoordinates(new Coordinates(_lat, _long));
                    var first = addresses.first;
                    List<Placemark> placemark = await Geolocator().placemarkFromCoordinates(_lat, _long);
                    setState(() {
                      isMove = false;
                      address = first.addressLine ;
                    });
                  },
                  onCameraMoveStarted: (){
                    setState(() {
                      isMove = true;
                      address = (isLanguage != true)?"Memuat":"Loading";
                    });
                  },
                  onCameraMove: (position){
                    setState(() {
                      print(position);
                      _lat = position.target.latitude;
                      _long = position.target.longitude;
                    });
                  },
                ),
              ),
              Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 1.1,
                        height: MediaQuery.of(context).size.height / 16,
                        decoration: BoxDecoration(
                            color: Colors.white,
                          borderRadius: BorderRadius.circular(8)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: Center(child: Text(address)),
                        ),
                      ),
                      Icon((isMove != true)?Icons.location_on:Icons.location_off, size: 32, color: Colors.grey,),
                      SizedBox(height: MediaQuery.of(context).size.height / 11,)
                    ],
                  )
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.height / 18,
                    decoration: BoxDecoration(
                      color: Color(0xffe3cea3),
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(6.0),
                      color: Colors.transparent,
                      child: AnimatedSwitcher(
                          duration: Duration.zero,
                          child: InkWell(
                            onTap: () async{
                              Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: ShippingAddressPage(isLanguage, Position(latitude: _lat, longitude: _long))) );
                            },
                            splashColor: Colors.white,
                            borderRadius: BorderRadius.circular(6.0),
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 22.0, vertical: 4.0),
                                child: Text((isLanguage != true)?"Pilih Alamat":"Choose Address", style: GoogleFonts.poppins(
                                    textStyle: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold)),),
                              ),
                            ),
                          )
                      ),
                    )
                ),
              ),
            ],
          ),
        )
    );
  }
}
