import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce/ui/detail/artist_detail_activity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:share/share.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class CustomCard{
  static Widget trendCard(BuildContext context, text, bool isCurrency){
    double ratingCount = 5;
    return GestureDetector(
      onTap: () {
        Share.share("Look at this! Ayam Goreng Nelongso, Mulyosari on GoFood." + "\t" + "https://gofood.link/u/41AgM");
      },
      child: Container(
        height: MediaQuery.of(context).size.height / 4,
        width: MediaQuery.of(context).size.width / 3.2,
        margin: EdgeInsets.only(left: 8),
        decoration: BoxDecoration(
            color: Colors.white
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height / 8,
                  width: MediaQuery.of(context).size.width / 3.2,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage('assets/image/parfume_trend.png'),
                      )
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    RatingBar(
                      itemSize: 18,
                      initialRating: ratingCount,
                      ignoreGestures: true,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        print(rating);
                      },
                    ),
                    SizedBox(width: 4,),
                    Text(ratingCount.toString(), style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 10, color: Color(0xff872934))))
                  ],
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: 4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text("Scrett Parfume", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),)),
                  Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?300000:20), style: GoogleFonts.poppins(textStyle: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.grey, fontSize: 12),)),
                  Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?150000:10), style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 12),)),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  static Widget discountCard(BuildContext context, text, bool isCurrency){
    double ratingCount = 5;
    return GestureDetector(
      onTap: () {
//      Share.share("Look at this! Ayam Goreng Nelongso, Mulyosari on GoFood." + "\t" + "https://gofood.link/u/41AgM");
      },
      child: Container(
        margin: EdgeInsets.only(right: 8, left: 8),
        width: MediaQuery.of(context).size.width / 3,
        height: MediaQuery.of(context).size.height / 6,
        decoration: BoxDecoration(
            color: Colors.white
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height / 8,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage('assets/image/parfume_trend.png'),
                      )
                  ),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.all(4),
                      child: Container(
                        height: MediaQuery.of(context).size.height/24,
                        width: MediaQuery.of(context).size.width/10,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xff672934),
                          border: Border.all(color: Colors.white, width: 1.5),
                        ),
                        child: Center(
                          child: Text("50%", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.white, fontSize: 10),)),
                        ),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    RatingBar(
                      itemSize: 18,
                      initialRating: ratingCount,
                      ignoreGestures: true,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        print(rating);
                      },
                    ),
                    SizedBox(width: 4,),
                    Text(ratingCount.toString(), style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 10, color: Color(0xff872934))))
                  ],
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: 4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text("Scrett Parfume", style: GoogleFonts.poppins(textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),)),
                  Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?300000:20), style: GoogleFonts.poppins(textStyle: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.grey, fontSize: 12),)),
                  Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?150000:10), style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 12),)),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  static Widget newProductCard(BuildContext context, bool isCurrency){
    double ratingCount = 5;
    return GestureDetector(
      onTap: () {
//      Share.share("Look at this! Ayam Goreng Nelongso, Mulyosari on GoFood." + "\t" + "https://gofood.link/u/41AgM");
      },
      child: Container(
          margin: EdgeInsets.only(right: 4, left: 4),
          width: MediaQuery.of(context).size.width / 1.2,
          height: MediaQuery.of(context).size.height / 3,
          decoration: BoxDecoration(
              color: Colors.white
          ),
          child: Row(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 3.2,
                height: MediaQuery.of(context).size.height / 4,
                decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage('assets/image/parfume_trend.png'),
                    ),
                    borderRadius: BorderRadius.circular(12)
                ),
              ),
              SizedBox(width: 8,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Intense", style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12),)),
                  Container(
                    width: MediaQuery.of(context).size.width / 2.5,
                    child: Text("[Lips Love] Intense Fluid Matte Lip Paint",
                        style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),)),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width / 8,
                        decoration: BoxDecoration(
                          color: Color(0xff672934)
                        ),
                        child: Center(
                            child: Text("-30%", style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12), color: Colors.white))
                        ),
                      ),
                      SizedBox(width: 4,),
                      Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?200000:13),
                          style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Color(0xff672934)),)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      RatingBar(
                        itemSize: 18,
                        initialRating: ratingCount,
                        ignoreGestures: true,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (rating) {
                          print(rating);
                        },
                      ),
                      SizedBox(width: 4,),
                      Text(ratingCount.toString(), style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 10, color: Color(0xff872934))))
                    ],
                  ),
                ],
              )
            ],
          )
      ),
    );
  }

  static Widget listHomeCard(BuildContext context, String image, bool isLanguage, bool isCurrency){
    double ratingCount = 5;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: (){

        },
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 3,
                offset: Offset(0, 0), // changes position of shadow
              ),
            ],
          ),
          child: new Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height / 6,
                width: MediaQuery.of(context).size.width / 2,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(image),
                    fit: BoxFit.cover
                  ),
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(8), topRight: Radius.circular(8))
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height / 5,
                width: MediaQuery.of(context).size.width / 2,
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          child: Text("[Lips] Natural Fluid From Lancome Germany",
                              style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),)),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          RatingBar(
                            itemSize: 18,
                            initialRating: ratingCount,
                            ignoreGestures: true,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                          SizedBox(width: 4,),
                          Text(ratingCount.toString(), style: GoogleFonts.poppins(
                              textStyle: TextStyle(fontSize: 10, color: Color(0xff872934))))
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Icon(Icons.location_on, color: Colors.grey, size: 12,),
                          SizedBox(height: 2,),
                          Text("Jakarta Utara", style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 10),)),
                        ],
                      ),
                      Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?150000:10), style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 14, fontWeight: FontWeight.bold),)),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text((isLanguage != true)?"Bebas":"Free", style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 10, fontWeight: FontWeight.bold),)),
                              Text((isLanguage != true)?"Ongkir":"Shipping", style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 10),)),
                            ],
                          ),
                          Image.asset("assets/image/icon_truck.png", height: 30,)
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  static Widget feedUpdateCard(BuildContext context, bool canReadMore, String descNewProduct, bool isLanguage, bool isCurrency){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: AssetImage("assets/image/seli_marchelia.png",
                      ),
                      radius: 18,
                    ),
                    SizedBox(width: 6,),
                    Text("Seli Marchelia", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w400,))),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Image.asset("assets/image/icon_bookmark.png", width: 20, height: 20,),
                    Text("1.5k", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                    Image.asset("assets/image/icon_ig.png", width: 20, height: 20,),
                    Text("10.1k", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                    SizedBox(width: 2,),
                    Image.asset("assets/image/icon_yt.png", width: 20, height: 20,),
                    Text("30.3k", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: 4,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Divider(thickness: 2,),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Text((isLanguage != true)?"Produk lipstik baru!":"New lipstick product!", style: GoogleFonts.poppins(
                textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Divider(thickness: 2,),
          ),
          SizedBox(height: 4,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Text((descNewProduct.length > 120 && canReadMore != false)?descNewProduct.substring(0, 119)
                  :(canReadMore != true)?descNewProduct:descNewProduct,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w400,))
              ),
            ),
          ),
          SizedBox(height: 4,),
          (descNewProduct.length > 120 && canReadMore != false)?Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: (){
                canReadMore = false;
              },
              child: Container(
                width: MediaQuery.of(context).size.width / 1.4,
                child: Text((isLanguage != true)?"Lebih banyak":"Read more", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, decoration: TextDecoration.underline))),
              ),
            ),
          ):Container(),
          SizedBox(height: 8,),
          //list product artist
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height/ 8,
            child: ListView.builder(
                itemCount: 5,
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index){
                  return CustomCard.newProductCard(context, isCurrency);
                }
            ),
          ),
        ],
      ),
    );
  }

  static Widget productArtistDetail(BuildContext context, bool isLanguage, bool isCurrency){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 6,
              width: MediaQuery.of(context).size.width / 2.8,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/image/parfume_trend.png"),
                      fit: BoxFit.cover
                  ),
                  borderRadius: BorderRadius.circular(14)
              ),
            ),
            SizedBox(width: 8,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text((isLanguage != true)?"23 Tahun":"23 Years old", style: GoogleFonts.poppins(textStyle: TextStyle(color: Colors.grey, fontSize: 14),)),
                Text("Black Parfume Original", style: GoogleFonts.poppins(textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)),
                Container(
                  height: MediaQuery.of(context).size.height / 32,
                  width: MediaQuery.of(context).size.width / 12,
                  decoration: BoxDecoration(
                      color: Color(0xff872934),
                      borderRadius: BorderRadius.circular(4)
                  ),
                  child: Center(
                    child: Text("20%", style: GoogleFonts.poppins(
                        textStyle: TextStyle(fontSize: 12, color: Colors.white, fontWeight: FontWeight.w500))),
                  ),
                ),
                Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?500000:33), style: GoogleFonts.poppins(textStyle: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.grey, fontSize: 14),)),
                Text(NumberFormat.currency(locale: (isCurrency != true)?'id':'en', symbol: (isCurrency != true)?'Rp ':r'$ ', decimalDigits: 0).format((isCurrency != true)?200000:13), style: GoogleFonts.poppins(textStyle: TextStyle(color: Color(0xff672934), fontSize: 14),)),
              ],
            ),
          ],
        ),
        SizedBox(height: 8,),
        Row(
          children: [
            Text((isLanguage != true)?"Ulasan Artis":"Artist Reviews ", style: GoogleFonts.poppins(
                textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))),
            Text("10k", style: GoogleFonts.poppins(
                textStyle: TextStyle(fontSize: 12, color: Color(0xff872934), fontWeight: FontWeight.w500))),
          ],
        ),
        Divider(thickness: 2,)
      ],
    );
  }

  static reviewProductDetailCard(BuildContext context, int index, bool isLanguage){
    double ratingCount = 5;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        (index == 0)?Container(
          height: MediaQuery.of(context).size.height / 24,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              color: Color(0xffe3cea3),
            borderRadius: BorderRadius.circular(6)
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text((isLanguage != true)?"Ulasan pengguna":"User's review", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold))),
                SizedBox(width: 8,),
                Text("(10)", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Color(0xff872934))))
              ],
            ),
          ),
        ):Container(),
        SizedBox(height: (index == 0)?4:0,),
        Text("Rahell", style: GoogleFonts.poppins(
            textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold))),
        (index == 2)?Container(
          height: MediaQuery.of(context).size.height /4,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/image/parfume_trend.png"),
              fit: BoxFit.cover
            )
          ),
        ):Container(),
        Text("Really high quality product :)", style: GoogleFonts.poppins(
            textStyle: TextStyle(fontSize: 12))),
        SizedBox(height: 8,),
        Text("P2. Don't Look Back", style: GoogleFonts.poppins(
            textStyle: TextStyle(fontSize: 12, color: Colors.grey))),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            RatingBar(
              itemSize: 18,
              initialRating: ratingCount,
              ignoreGestures: true,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
            SizedBox(width: 4,),
            Text(ratingCount.toString(), style: GoogleFonts.poppins(
                textStyle: TextStyle(fontSize: 10, color: Color(0xff872934)))),
            SizedBox(width: 4,),
            Text("|", style: GoogleFonts.poppins(
                textStyle: TextStyle(fontSize: 10, color: Color(0xff872934)))),
            SizedBox(width: 4,),
            Text("10. 08. 2020", style: GoogleFonts.poppins(
                textStyle: TextStyle(fontSize: 10, color: Color(0xff872934))))
          ],
        ),
        Divider(thickness: 1,)
      ],
    );
  }

  static Widget listInFeedExploreCard(BuildContext context, String image){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 2),
      child: Container(
        width: MediaQuery.of(context).size.width/10.5,
        height: MediaQuery.of(context).size.height/10.5,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(image)
            )
        ),
      ),
    );
  }

}

class FeedUpdateCard extends StatefulWidget {
  bool canReadMore;
  bool contentVideo;
  String descNewProduct;
  String urlYoutube;
  bool isLanguage;
  bool isCurrency;
  FeedUpdateCard(this.canReadMore, this.contentVideo, this.descNewProduct, this.urlYoutube, this.isLanguage, this.isCurrency);

  @override
  _FeedUpdateCardState createState() => _FeedUpdateCardState(canReadMore, contentVideo, descNewProduct, urlYoutube, isLanguage, isCurrency);
}

class _FeedUpdateCardState extends State<FeedUpdateCard> {
  bool canReadMore;
  bool contentVideo;
  String descNewProduct;
  String urlYoutube;
  bool isLanguage;
  bool isCurrency;
  _FeedUpdateCardState(this.canReadMore, this.contentVideo, this.descNewProduct, this.urlYoutube, this.isLanguage, this.isCurrency);

  VideoPlayerController _controller;
  YoutubePlayerController _controllerYoutube;
  Future<void> _intializeVideoPlayerFuture;

  bool isVideoEnd = false;
  String videoYoutubeURL;
  int _currentArtist = 0;

  List carouselArtist = [
    "assets/image/seli_marchelia.png",
    "assets/image/seli_marchelia.png",
    "assets/image/seli_marchelia.png",
    "assets/image/seli_marchelia.png",
  ];

  List<T> mapArtist<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  _launchURL() async {
    const url = 'https://vt.tiktok.com/yx87JR/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    _controller = VideoPlayerController.network("https://v16m.tiktokcdn.com/752484edad5e7128b5893941bfd1d19b/5f47369b/video/tos/alisg/tos-alisg-pve-0037/b377e158bc9d410c8f44f37a56a02060/?a=1180&br=1426&bt=713&cr=0&cs=0&dr=3&ds=3&er=&l=202008250429150101151192350201DE09&lr=tiktok&mime_type=video_mp4&qs=0&rc=anFoajNqNzd2djMzMzgzM0ApNjQ1PDVkOTw0N2VpNzozZ2dnYi9zaHExcTZfLS01LzRzc2FiY2BeXzMyX18yMTYxLmE6Yw%3D%3D&vl=&vr=");
    _controller.addListener(() {
      if(_controller.value.position == _controller.value.duration) {
        setState(() {
          isVideoEnd = true;
        });
      }
    });
    _controller.pause();
    _controller.setLooping(false);
    _controller.setVolume(1.0);

    setState(() {
      videoYoutubeURL = (urlYoutube != "")?urlYoutube:"https://www.youtube.com/watch?v=ApXoWvfEYVU";
    });

    _controllerYoutube = YoutubePlayerController(
      initialVideoId: YoutubePlayer.convertUrlToId(videoYoutubeURL),
      flags: YoutubePlayerFlags(
        autoPlay: false,
        forceHD: false,
        loop: false
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    _controllerYoutube.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
            (contentVideo != false)?Stack(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 1.2,
                  child: FutureBuilder(
                    future: _intializeVideoPlayerFuture,
                    builder: (context, snapshot){
                      if(snapshot.connectionState == ConnectionState.done){
                        return AspectRatio(
                          aspectRatio: _controller.value.aspectRatio,
                          child: VideoPlayer(_controller),
                        );
                      }else{
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  ),
                ),
                (_controller.value.isPlaying != true)?Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 1.2,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(.5)
                  ),
                  child: Center(
                    child: GestureDetector(
                      onTap: (){
                        setState(() {
                          if(isVideoEnd != false){
                            isVideoEnd = false;
                            _controller.initialize();
                            _controller.play();
                          }else if(_controller.value.isPlaying != true){
                            _intializeVideoPlayerFuture = _controller.initialize();
                            _controller.play();
                          }
                        });
                      },
                        child: Image.asset("assets/image/icon_play.png", height: MediaQuery.of(context).size.height / 8,)),
                  ),
                ):Container(),
              ],
            ):GestureDetector(
              onTap: _launchURL,
              child: Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 3,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/image/seli_marchelia.png"),
                            fit: BoxFit.cover
                        )
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 3,
                    child: Padding(
                      padding: const EdgeInsets.all(80.0),
                      child: Image.asset("assets/image/icon_tiktok.png"),
                    ),
                  ),
                ],
              ),
            ),
//          (contentVideo != false)?
//          YoutubePlayer(
//            controller: _controllerYoutube,
//            progressIndicatorColor: Colors.red,
//            liveUIColor: Colors.red,
//            showVideoProgressIndicator: true,
//            bottomActions: [
//              CurrentPosition(controller: _controllerYoutube,),
//              ProgressBar(isExpanded: true, controller: _controllerYoutube,
//                colors: ProgressBarColors(playedColor: Colors.red, handleColor: Colors.red),),
//              RemainingDuration(controller: _controllerYoutube,),
//            ],
//          ):Container(
//            width: MediaQuery.of(context).size.width,
//            child: Stack(
//              children: <Widget>[
//                CarouselSlider(
//                  options: CarouselOptions(
//                      height: MediaQuery.of(context).size.height / 3.5,
//                      scrollDirection: Axis.horizontal,
//                      enableInfiniteScroll: true,
//                      onPageChanged: (index, reason){
//                        setState(() {
//                          _currentArtist = index;
//                        });
//                      }
//                  ),
//                  items: carouselArtist.map((e) {
//                    return Container(
//                      width: MediaQuery.of(context).size.width,
//                      decoration: BoxDecoration(
//                          image: DecorationImage(
//                            image: AssetImage(e),
//                            fit: BoxFit.fill,
//                          )
//                      ),
//                    );
//                  }).toList(),
//                ),
//                Positioned(
//                  left: MediaQuery.of(context).size.width / 2.4,
//                  bottom: 4,
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    crossAxisAlignment: CrossAxisAlignment.end,
//                    children: mapArtist<Widget>(carouselArtist, (index, url) {
//                      return Container(
//                        width: _currentArtist == index ? 12 : 10.0,
//                        height: _currentArtist == index ? 12 : 10.0,
//                        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
//                        decoration: BoxDecoration(
//                          shape: BoxShape.circle,
//                          color: _currentArtist == index ? Color(0xff872934) : Colors.white,
//                        ),
//                      );
//                    }),
//                  ),
//                ),
//              ],
//            ),
//          ),
          SizedBox(height: 8,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: (){
                Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: new ArtistDetailActivity(isLanguage, isCurrency)) );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: AssetImage("assets/image/seli_marchelia.png",
                        ),
                        radius: 18,
                      ),
                      SizedBox(width: 6,),
                      Text("Seli Marchelia", style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w400,))),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Image.asset("assets/image/icon_selected_bookmark.png", width: 20, height: 20,),
                      Text("1.5k", style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                      SizedBox(width: 4,),
                      Image.asset("assets/image/icon_ig.png", width: 20, height: 20,),
                      Text("10.1k", style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                      SizedBox(width: 4,),
                      Image.asset("assets/image/icon_yt.png", width: 20, height: 20,),
                      Text("30.3k", style: GoogleFonts.poppins(
                          textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                    ],
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 4,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Divider(thickness: 2,),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Text((isLanguage != true)?"Produk lipstik baru":"New lipstick product!", style: GoogleFonts.poppins(
                textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Divider(thickness: 2,),
          ),
          SizedBox(height: 4,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Text((descNewProduct.length > 120 && canReadMore != false)?descNewProduct.substring(0, 119)
                  :(canReadMore != true)?descNewProduct:descNewProduct,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w400,))
              ),
            ),
          ),
          SizedBox(height: 4,),
          (descNewProduct.length > 120 && canReadMore != false)?Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: (){
                setState(() {
                  canReadMore = false;
                });
              },
              child: Container(
                width: MediaQuery.of(context).size.width / 1.4,
                child: Text((isLanguage != true)?"Lebih banyak":"Read more", style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, decoration: TextDecoration.underline))),
              ),
            ),
          ):Container(),
          SizedBox(height: 8,),
          //list product artist
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height/ 2.58,
            child: ListView.builder(
                itemCount: 5,
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index){
                  return CustomCard.listHomeCard(context, "assets/image/parfume_trend.png", isLanguage, isCurrency);
                }
            ),
          ),
        ],
      ),
    );
  }
}

class FeedExploreCard extends StatefulWidget {
  bool isLanguage;

  FeedExploreCard(this.isLanguage);

  @override
  _FeedExploreCardState createState() => _FeedExploreCardState(isLanguage);
}

class _FeedExploreCardState extends State<FeedExploreCard> {
  bool isLanguage;

  _FeedExploreCardState(this.isLanguage);

  double ratingCount = 5;

  @override
  Widget build(BuildContext context) {
//    List<Widget> _tiles = [
//      CustomCard.listInFeedExploreCard(context, "assets/image/parfume_trend.png"),
//      CustomCard.listInFeedExploreCard(context, "assets/image/parfume_trend.png"),
//      CustomCard.listInFeedExploreCard(context, "assets/image/parfume_trend.png"),
//      CustomCard.listInFeedExploreCard(context, "assets/image/parfume_trend.png"),
//    ];
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white ,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 3,
              offset: Offset(0, 0), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 5,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/image/seli_marchelia.png"),
                      fit: BoxFit.cover
                  )
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Angel Ramos", style: GoogleFonts.poppins(
                      textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            children: [
                              Text((isLanguage != true)?"Pengunjung   ":"Visitor   ", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 10,))),
                              Text("30K", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                            ],
                          ),
                          Row(
                            children: [
                              Text((isLanguage != true)?"Produk  ":"Product  ", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 10,))),
                              Text("100", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Color(0xff872934)))),
                            ],
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 14),
                        child: GestureDetector(
                          onTap: (){

                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width/5,
                            height: MediaQuery.of(context).size.height/24,
                            decoration: BoxDecoration(
                                color: Color(0xff872934),
                                borderRadius: BorderRadius.circular(6)
                            ),
                            child: Center(
                              child: Text((isLanguage != true)?"Langganan":"Subscribe", style: GoogleFonts.poppins(
                                  textStyle: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Colors.white))),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height/ 16,
                      child: ListView.builder(
                        itemCount: 4,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index){
                            return CustomCard.listInFeedExploreCard(context, "assets/image/parfume_trend.png");
                        }
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

