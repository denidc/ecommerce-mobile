import 'package:ecommerce/model/search.dart';
import 'package:ecommerce/test.dart';
import 'package:ecommerce/ui/chat/chat_seller_activity.dart';
import 'package:ecommerce/ui/delivery_information/confirm_payment_activity.dart';
import 'package:ecommerce/ui/delivery_information/customer_information_activity.dart';
import 'package:ecommerce/ui/delivery_information/payment_activity.dart';
import 'package:ecommerce/ui/detail/artist_detail_activity.dart';
import 'package:ecommerce/ui/detail/product_detail_activity.dart';
import 'package:ecommerce/ui/feed/feed_explore_page.dart';
import 'package:ecommerce/ui/home/home_activity.dart';
import 'package:ecommerce/ui/login%20and%20regis/forgot_password_page.dart';
import 'package:ecommerce/ui/login%20and%20regis/login_activity.dart';
import 'package:ecommerce/ui/main_activity.dart';
import 'package:ecommerce/ui/product/product_activity.dart';
import 'package:ecommerce/ui/search/search_activity.dart';
import 'file:///C:/Users/user/AndroidStudioProjects/ecommerce/lib/ui/setting/user/cart_shop_page.dart';
import 'file:///C:/Users/user/AndroidStudioProjects/ecommerce/lib/ui/setting/user/edit_password_page.dart';
import 'file:///C:/Users/user/AndroidStudioProjects/ecommerce/lib/ui/setting/user/history_order_page.dart';
import 'file:///C:/Users/user/AndroidStudioProjects/ecommerce/lib/ui/setting/user/points_page.dart';
import 'package:ecommerce/ui/setting/setting_activity.dart';
import 'package:ecommerce/ui/setting/store/verification_phone_page.dart';
import 'file:///C:/Users/user/AndroidStudioProjects/ecommerce/lib/ui/setting/user/shipping_address_page.dart';
import 'package:ecommerce/ui/splash_screen/splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await FlutterDownloader.initialize(debug: true);

  OneSignal.shared.init(
      "b2aa025d-9354-42c8-ba6e-4a981b0c595e",
      iOSSettings: null
  );
//  OneSignal.shared.setNotificationReceivedHandler((OSNotification notification) {
//    // will be called whenever a notification is received
//    print(notification);
//    if(notification.androidNotificationId != null){
//      Future.delayed(Duration(seconds: 5), ()async{
//        var status = await OneSignal.shared.getPermissionSubscriptionState();
//
//        var playerId = status.subscriptionStatus.userId;
//
//        await OneSignal.shared.postNotification(OSCreateNotification(
//          playerIds: ["b144368d-4de4-49c5-aaa7-c72c3bf2db49"],
//          heading: "Schedule",
//          content: "Lima Detik Terlewati",
//          androidChannelId: "9a767e3b-b890-4438-aa1b-a593ee86834e",
//        ));
//      });
//    }
//  });
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}

